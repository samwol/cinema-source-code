<?php
include('../../../common/basic.php');
include('../admincore/user_other.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>用户相关设置</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/layui.css" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	
</head>
<body class="childrenBody layui-form">
		 
		<div class="layui-form-item">	  
		<div class="jsxs-sc">
			<div class="layui-row">
				<input type="file" accept="image/*" multiple="">
				<button type="button" class="layui-btn">选择本地图片</button>
				<button type="button" class="layui-btn layui-btn-normal" onclick="url_uploaddj()">上传远程图片</button>
				<a class="layui-btn layui-btn-danger" style="height:50px; line-height:50px;"  href="https://imgchr.com/"/>备用图床</a>
			</div>
		</div><br>
         <div class="layui-form-item">
				    <div class="layui-input-block">
				      <textarea id="url-res-txt"  rows="5" placeholder="上传后的图片外链地址将显示在此处哦！" class="layui-textarea"></textarea>
				    </div>
			</div>
		
		<div class="preview">
		</div>			  				  
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://img.oioweb.cn/js/index.js?v=1.0"></script>
</body>
</html>