<?php
include('../../../common/basic.php');
include('../admincore/user_edit.php');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>编辑用户</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
    <link rel="stylesheet" href="../../css/user.css" media="all" />
	<script type="text/javascript" src="../../js/jquery.min.js" ></script>
</head>
<body class="childrenBody">


<section class="layui-larry-box">
	<div class="user_left">
		<div class="larry-personal-body clearfix changepwd">
		<?php
					$result = mysql_query('select * from aikcms_user where id = '.$_GET['id'].' ');	
					if ($row = mysql_fetch_array($result)){
					?>
			<form class="layui-form col-lg-5" method="post"  enctype="multipart/form-data" >		
			<div class="layui-form-item">
					<label class="layui-form-label">用户头像</label>
					<div class="layui-input-block">
						<div class="layui-box layui-upload-button"><input type="file" name="aik_user_img" accept="image/*" class="layui-upload-file"  onchange="changImg(event)"><span class="layui-upload-icon"><i class="layui-icon"></i>上传图片</span></div>
					</div>
					<div   class="layui-input-block" style="height:100px; width:100px; border-radius:50%; overflow:hidden;border: 1px solid #e6e6e6;">  
						<img  id="myImg" alt="暂无图片" src="<?php echo $row['aik_user_img'];?>"  style="height:100%; width:100%;border:0; line-height:100px;text-align:center;">
					</div>
					
				</div>				
		<div class="layui-form-item">	
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_user_name"  autocomplete="off"  class="layui-input" value="<?php echo $row['aik_user_name'];?>" >
					</div> 
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">密码</label>
<div class="layui-input-block">  
						<input type="password" name="aik_user_pw"  autocomplete="off"  class="layui-input">
					</div> 	
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">用户QQ</label>
<div class="layui-input-block">  
						<input type="text" name="aik_user_qq"  autocomplete="off"  class="layui-input" value="<?php echo $row['aik_user_qq'];?>">
					</div> 	
		</div><div class="layui-form-item">	
				<label class="layui-form-label">用户组</label>
				<div class="layui-inline">	<div class="layui-input-inline">
					<select name="aik_user_group" class="newsLook" lay-filter="browseLook">		
					    <option value="0" <?php echo  $a = $row['aik_user_group']=="0" ? "selected='selected'" :"";?>>普通会员</option>
						<?php echo usergroup_select_list($row['aik_user_group'])?>
				    </select>
				</div>
				</div>
			<div class="layui-inline">		
				    <label class="layui-form-label">修改用户组</label>
				    <div class="layui-input-block">
				      <input type="radio" name="aik_group" value="1" title="是">
				      <input type="radio" name="aik_group" value="0" title="否" checked>
				    </div>
			</div>
		</div><div class="layui-form-item">	
				<label class="layui-form-label">初始金额</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_user_allmoney"  autocomplete="off"  class="layui-input" value="<?php echo $row['aik_user_allmoney'];?>">
					</div> 
		</div><div class="layui-form-item">	
				<label class="layui-form-label">初始积分</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_user_int"  autocomplete="off"  class="layui-input" value="<?php echo $row['aik_user_int'];?>">
					</div> 
		</div>
		
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="update" >点击修改</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
					</form><?php }?>
					</div>
	</div>
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../admincore/addtime.js"></script>
<script type="text/javascript">  
     function changImg(e){  
        for (var i = 0; i < e.target.files.length; i++) {  
            var file = e.target.files.item(i);  
            if (!(/^image\/.*$/i.test(file.type))) {  
                continue; //不是图片 就跳出这一次循环  
            }  
            //实例化FileReader API  
            var freader = new FileReader();  
            freader.readAsDataURL(file);  
            freader.onload = function(e) {  
                $("#myImg").attr("src",e.target.result);  
            }  
        }  
    }  
</script> 
</body>
</html>