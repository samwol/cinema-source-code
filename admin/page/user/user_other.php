<?php
include('../../../common/basic.php');
include('../admincore/user_other.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>用户相关设置</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody layui-form">
<blockquote class="layui-elem-quote title">用户相关设置</blockquote><br>
<?php
$result = mysql_query('select * from aikcms_basic where id = 1');
					if( $row = mysql_fetch_array($result)){
					?>
		<form method="post">
			 <div class="layui-form">
			 	<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">用户注册</label>
			<div class="layui-input-block">
				      <input type="radio" name="aik_user_open" value="1" title="开" <?php echo $a = $row['aik_user_open']==1 ? 'checked' :''; ?> >
				      <input type="radio" name="aik_user_open" value="0" title="关" <?php echo $a = $row['aik_user_open']==0 ? 'checked' :''; ?> >
				    </div>
			</div>
		</div>	
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">用户初始积分</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_user_initint" placeholder="用户初始积分" type="text" value='<?php echo $row['aik_user_initint'];?>'>
				</div>
			</div>
		</div><div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">积分兑换比例</label>
			 <div class="layui-input-inline">
				      <input type="text" name="aik_user_intra"  placeholder="即一元可购买多少积分" autocomplete="off" class="layui-input" value="<?php echo $row['aik_user_intra']?>">
				    </div><div class="layui-form-mid layui-word-aux">即一元可购买多少积分，如1RMB买10个积分，就填入10！</div>
			</div>
		</div> <div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">推广积分</label>
			 <div class="layui-input-inline">
				      <input type="text" name="aik_user_affint"  placeholder="即一元可购买多少积分" autocomplete="off" class="layui-input" value="<?php echo $row['aik_user_affint']?>">
				    </div><div class="layui-form-mid layui-word-aux">即推广一人可得到多少积分！</div>
			</div>
		</div> 
		<blockquote class="layui-elem-quote title">卡密相关设置</blockquote><br>
		 <div class="layui-form-item">
				    <label class="layui-form-label">卡密购买链接</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_buycard"  placeholder="请输入卡密购买链接"  class="layui-input" value="<?php echo $row['aik_buycard']?>">
				    </div>
				  </div>
		<div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" name="update">立即提交</button>
				    </div>
				  </div>
		</div>
					</form><?php }?>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>