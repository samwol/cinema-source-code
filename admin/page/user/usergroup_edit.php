<?php
include('../../../common/basic.php');
include('../admincore/usergroup_edit.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>用户组编辑</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
	<script type="text/javascript" src="../../js/jquery.min.js" ></script>
</head>
<body class="childrenBody">
<?php
					$result = mysql_query('select * from aikcms_usergroup where id = '.$_GET['id'].' ');	
					if ($row = mysql_fetch_array($result)){
					?>
	<form class="layui-form" method="post" enctype="multipart/form-data" >
		<div class="layui-form-item">
			<label class="layui-form-label">用户组名称</label>
			<div class="layui-input-block">
				<input class="layui-input newsName" name="aik_usergroup_name" placeholder="用户组名称" type="text" value="<?php echo $row['aik_usergroup_name'];?>">
			</div>
		</div>
			<div class="layui-form-item">
					<div class="layui-inline">
					<label class="layui-form-label">用户组图标</label>
						<div class="layui-box layui-upload-button"><input type="file" name="aik_usergroup_img" accept="image/*" class="layui-upload-file"  onchange="changImg(event)"><span class="layui-upload-icon"><i class="layui-icon"></i>上传图片</span></div></div>
					<div class="layui-inline" style=" width:100px; height:36px; hidden;border: 1px solid #e6e6e6;">  
						<img  id="myImg" alt="暂无图片" src="<?php echo $row['aik_usergroup_img'];?>" style="height:100%; width:100%;border:0; line-height:36px; text-align:center;">
					</div>
				</div>	
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">购买价格</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_usergroup_price" placeholder="请输入价格" type="text" value="<?php echo $row['aik_usergroup_price'];?>">
				</div>
			</div>
			<div class="layui-inline">		
			<label class="layui-form-label">用户组时长</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_usergroup_length" placeholder="请输入数字" type="text"  value="<?php echo $row['aik_usergroup_length'];?>">
				</div><p style="color:red; float:left;line-height:38px;">*仅填天数，永久请填0！</p>
		</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">备注</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_usergroup_rem" placeholder="请输入备注" type="text" value="<?php echo $row['aik_usergroup_rem'];?>">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="update">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div><br>
					</form><?php }?>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript">  
     function changImg(e){  
        for (var i = 0; i < e.target.files.length; i++) {  
            var file = e.target.files.item(i);  
            if (!(/^image\/.*$/i.test(file.type))) {  
                continue; //不是图片 就跳出这一次循环  
            }  
            //实例化FileReader API  
            var freader = new FileReader();  
            freader.readAsDataURL(file);  
            freader.onload = function(e) {  
                $("#myImg").attr("src",e.target.result);  
            }  
        }  
    }  
</script> 
</body>
</html>