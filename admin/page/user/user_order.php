<?php
include('../../../common/basic.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>用户管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
		
		<div class="layui-inline"><form method="get" >
		    <div class="layui-input-inline">
		    	<input  name="wd" placeholder="请输入用户名" class="layui-input search_input" type="text" >
		    </div><button id="search" class="layui-btn" type="submit" >查询</button>
		</form></div>
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list"><form method="post">
                    <table class="layui-table">
					    <colgroup>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
						<tr>
							<th style="text-align:left;">订单号</th>
							<th>用户</th>
							<th>充值金额</th>
							<th>交易状态</th>
							<th>付款方式</th>
							<th>创建时间</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
					<?php

									$sql = 'select * from aikcms_user_pay order by id desc';
									$pager = page_handle('page',20,mysql_num_rows(mysql_query($sql)));
									$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
				
							if (isset($_GET['wd'])) {
								$sql = 'select * from aikcms_user_pay where aik_pay_userid = "'.get_user_id($_GET['wd']).'" order by id desc';
								$pager = page_handle('page',20,mysql_num_rows(mysql_query($sql)));
								$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							}
							while($row= mysql_fetch_array($result)){
							?>
						<tr>
                            <td><?php echo $row['aik_pay_order']?></td>
                            <td><?php echo get_user_name($row['aik_pay_userid'])?></td>
                            <td><?php echo $row['aik_pay_num']?></td>
                            <td><?php echo $row['aik_pay_state'] = $row['aik_pay_state']=="0" ? '<font color="red">未付款</font>' : '<font color="green">已付款</font>'; ?></td>
                            <td><?php if($row['aik_pay_mode']=='ALIPAY'){echo '支付宝';}elseif($row['aik_pay_mode']=='WEIXINPAY'){echo '微信';}else{echo 'QQ钱包';}?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['aik_pay_time'])?></td>
						</tr>
						
						<?php } ?>
					</tbody>	
					</table>
			<div class="layui-form-item">
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div>
			     <!-- 登录日志 -->
			    
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>