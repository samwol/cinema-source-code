<?php
include('../../../common/basic.php');
include('../admincore/notice.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>公告管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
        <blockquote class="layui-elem-quote news_search">
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal noticeAdd_btn">发布公告</a>
		</div>
		
	</blockquote>          
		         <!-- 操作日志 -->
				 
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col width="200">
					</colgroup>
					<thead>
						<tr>
						    <th>ID</th>
							<th style="text-align:left;">公告内容</th>
							<th>公告链接</th>
							<th>发布时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$result = mysql_query('select * from aikcms_notice order by id desc');
						while($row = mysql_fetch_array($result)){
						?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_notice_title'];?></td>
							<td><?php echo $a = $row['aik_notice_url']=="" ? "空" : $row['aik_notice_url'];?></td>
							<td><?php echo date("Y-m-d H:i:s",$row['aik_notice_time']);?></td>
							<td>
								<a class="layui-btn layui-btn-mini"  href="notice_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>" onclick="return confirm('确认要删除吗？')"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>
			    </div>
			     <!-- 登录日志 -->
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>