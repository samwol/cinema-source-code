<?php
include('../../../common/basic.php');
include('../admincore/card_group_add.php');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>生成卡密</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/user.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">


<section class="layui-larry-box">
	<div class="user_left">
		<div class="larry-personal-body clearfix changepwd">
		
			<form class="layui-form col-lg-5" method="post"  enctype="multipart/form-data" >				
<div class="layui-form-item">	
				<label class="layui-form-label">用户组</label>
				<div class="layui-input-inline">
					<select name="aik_card_group" class="newsLook" lay-filter="browseLook">
					<?php
							$result = mysql_query('select * from aikcms_usergroup');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['id']?>"><?php echo $row['aik_usergroup_name']?></option>
                     <?php }?>
				    </select>
				</div>
			
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">卡密长度</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_card_long"  autocomplete="off"  class="layui-input" value="20">
					</div> 
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">生成数量</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_card_num"  autocomplete="off"  class="layui-input" value="1">
					</div> 
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save" >点击生成</button>
		    </div>
		</div></br>
			</form>
					</div>
	</div>
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>