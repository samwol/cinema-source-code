<?php
include('../../../common/basic.php');
include('../admincore/card.php');
?>
<script type="text/javascript">
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>卡密管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
		<form method="post">
		<div class="layui-inline">
		    <div class="layui-input-inline">
		    	<input  name="card" placeholder="请输入要查询的卡密" class="layui-input search_input" type="text" >
		    </div><button id="search" class="layui-btn" type="submit" >查询</button>
		</div><div class="layui-inline">
			<a class="layui-btn layui-btn-normal cardgAdd_btn">生成用户组升级卡密</a>
			<a class="layui-btn layui-btn-normal cardiAdd_btn" style="background-color:#5FB878">生成积分充值卡密</a>
			<input type="submit" id="execute" class="layui-btn layui-btn-danger" name="execute"  value="批量删除" />
		</div>
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list">
                    <table class="layui-table">
					<thead>
						<tr>
							<th style="width:50px;"><input name="" lay-skin="primary" lay-filter="allChoose" id="allChoose" type="checkbox">
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</th>
							<th style="text-align:left;">ID</th>
							<th style="text-align:left;">卡密</th>
							<th>可升级用户组</th>
							<th>可充值积分</th>
							<th>生成/使用时间</th>
							<th>使用用户</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
				<?php

									$sql = 'select * from aikcms_card order by id desc';
									$pager = page_handle('page',10,mysql_num_rows(mysql_query($sql)));
									$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							if (isset($_POST['card'])) {
								$sql = 'select * from aikcms_card where aik_card like "%'.$_POST['card'].'%" order by id desc';
								$pager = page_handle('page',20,mysql_num_rows(mysql_query($sql)));
								$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							}
							while($row= mysql_fetch_array($result)){
							?>
						<tr>
							<td><input name="id[]" type="checkbox" value="<?php echo $row['id'] ?>" />
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</td>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_card'];?></td>
							<td><?php echo $a = $row['aik_card_group']=='' ? '无':get_usergroup_name($row['aik_card_group']); ?></td>
							<td><?php echo $b = $row['aik_card_int']=='' ? '无':$row['aik_card_int']; ?></td>
							<td><?php $time = $row['aik_card_user']=='' ? $row['aik_card_time']:$row['aik_card_utime'];
							echo date('Y-m-d H:i:s',$time);?></td>
							<td><?php echo $c = $row['aik_card_user']=='' ? '<span class="user_on">未使用</span>':$row['aik_card_user']; ?></td>
							<td>
								<a class="layui-btn layui-btn-danger layui-btn-mini"  href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						
						<?php } ?>
					</tbody>	
					</table>
			<div class="layui-form-item">
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div></form>
			     <!-- 登录日志 -->
			    
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>