<?php
include('../../../common/basic.php');
include('../admincore/notice_add.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>发布公告</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">
	<form class="layui-form" method="post">
		<div class="layui-form-item">
			<label class="layui-form-label">公告内容</label>
			<div class="layui-input-block">
				<input class="layui-input newsName" name="aik_notice_title" lay-verify="required" placeholder="请输入公告内容" type="text">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">公告链接</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_notice_url" placeholder="前面一定要带http://" type="text">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save" >立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
	</form>
<script type="text/javascript" src="../../layui/layui.js"></script>

</body>
</html>