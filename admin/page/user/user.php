<?php
include('../../../common/basic.php');
include('../admincore/user.php');
?>
<script type="text/javascript">
	$(function(){
		$('#execute').click(function(){
			if ($('#execute_method').val() == ''){
				alert('请选择一项要执行的操作！');
				return false;
			};
			if ($('input[name="id[]"]').val() = ''){
				alert('请至少选择一项！');
				return false;
			};
		});
	});
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>用户管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
		
		<div class="layui-inline"><form method="get" >
		    <div class="layui-input-inline">
		    	<input  name="wd" placeholder="请输入用户名" class="layui-input search_input" type="text" >
		    </div><button id="search" class="layui-btn" type="submit" >查询</button>
		</form></div><div class="layui-inline">
			<a class="layui-btn layui-btn-normal userAdd_btn">添加用户</a>
		</div>
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list"><form method="post">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col width="200">
					</colgroup>
					<thead>
						<tr>
							<th><input name=""  lay-filter="allChoose" id="allChoose" type="checkbox">
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</th>
							<th style="text-align:left;">用户名</th>
							<th>用户QQ</th>
							<th>余额</th>
							<th>积分</th>
							<th>用户分组</th>
							<th>允许登陆</th>
							<th>注册时间</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
					<?php

									$sql = 'select * from aikcms_user order by id desc';
									$pager = page_handle('page',10,mysql_num_rows(mysql_query($sql)));
									$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
				
							if (isset($_GET['wd'])) {
								$sql = 'select * from aikcms_user where aik_user_name like "%'.$_GET['wd'].'%" order by id desc';
								$pager = page_handle('page',20,mysql_num_rows(mysql_query($sql)));
								$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							}
							while($row= mysql_fetch_array($result)){
							?>
						<tr>
							<td><input name="id[]" type="checkbox" value="<?php echo $row['id'] ?>" />
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</td>
							<td align="left"><?php echo $row['aik_user_name'];?></td>
							<td><?php echo $a = $row['aik_user_qq']=="" ? '暂无' : $row['aik_user_qq'];?></td>
							<td><?php echo ($row['aik_user_allmoney']-$row['aik_user_usedmoney'])?>元</td>
							<td><?php echo $row['aik_user_int'];?></td>
							<td><?php echo get_usergroup_name($row['aik_user_group'])?></td>
							<td><?php echo $a = $row['aik_user_on']=="1" ? '<span class="user_on">ON</span>' : '<span class="user_off">OFF</span>';?></td>
							<td><?php echo date("Y-m-d H:i:s",$row['aik_user_time']);?></td>
							<td>
								<a class="layui-btn layui-btn-mini" href="user_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini"  href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						
						<?php } ?>
					</tbody>	
					</table>
			<div class="layui-form-item">
				<div class="layui-inline">		
					<select id="execute_method" class="bk-margin-5 btn btn-primary" name="execute_method">
										<option value="" >请选择操作</option>
										<option value="del">删除选中</option>
										<option value="off">锁定用户</option>
										<option value="on">解锁用户</option>
									</select>
			</div><div class="layui-inline">	
				 <input type="submit" id="execute" class="layui-btn layui-btn-danger" name="execute" onclick="return confirm('确定要进行操作吗')" value="确定" />
			</div>
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div></form>
			     <!-- 登录日志 -->
			    
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>