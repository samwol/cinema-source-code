<?php
include('../../../common/basic.php');
include('../admincore/user_add.php');
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>添加用户</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
	<link rel="stylesheet" href="../../css/user.css" media="all" />
</head>
<body class="childrenBody">


<section class="layui-larry-box">
	<div class="user_left">
		<div class="larry-personal-body clearfix changepwd">
		
			<form class="layui-form col-lg-5" method="post">					
		<div class="layui-form-item">	
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_user_name"  placeholder="请输入用户名"  class="layui-input"  >
					</div> 
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">密码</label>
<div class="layui-input-block">  
						<input type="password" name="aik_user_pw"  placeholder="请输入密码"  class="layui-input" >
					</div> 	
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">用户QQ</label>
<div class="layui-input-block">  
						<input type="text" name="aik_user_qq" placeholder="请输入用户QQ"  class="layui-input" >
					</div> 	
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">用户组</label>
<div class="layui-input-block">  
						<select name="aik_user_group" class="newsLook" lay-filter="browseLook">
					<option value="0">普通会员</option>
					<?php
							$result = mysql_query('select * from aikcms_usergroup');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['id']?>"><?php echo $row['aik_usergroup_name']?></option>
                     <?php }?>
				    </select>
					</div> 	
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">初始金额</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_user_allmoney"  placeholder="请输入用户的初始金额"  class="layui-input"  value="0">
					</div> 
		</div><div class="layui-form-item">	
				<label class="layui-form-label">初始积分</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_user_int"  placeholder="请输入用户的初始积分"  class="layui-input" value="0">
					</div> 
		</div>
		
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save" >点击添加</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
			</form>
					</div>
	</div>
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../admincore/addtime.js"></script>
</body>
</html>