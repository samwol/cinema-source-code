﻿<?php
include('../../common/basic.php');
$infoo=file_get_contents('http://www.360kan.com/dianshi/list.php');
$count='#安装360影视大全APP，免费观看<span>(.*?)</span>部热门大片#';
preg_match_all($count,$infoo,$coarr);
$num=$coarr[1][0];
$url='http://130ak.com/';
$info=file_get_contents($url);
$surl='#<article[\s\S]*?</article>#';
preg_match_all($surl,$info,$arr);
$list = $arr[0][0];
$name='#<a href="(.*?)">(.*?)</a>#';
$name_result = array(
		'link'=>1,
		'title'=>2,
	);
preg_match_all($name, $list, $namearr);
foreach($namearr[$name_result['link']] as $k=>$c){
		$resultnm[$k]['link'] = $namearr[$name_result['link']][$k];
		$resultnm[$k]['title'] = $namearr[$name_result['title']][$k];
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>首页--AikCms后台管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../css/main.css" media="all" />
</head>
<body class="childrenBody">
	<div class="panel_box row">
		<div class="panel col">
			<a href="javascript:;" data-url="page/message/message.php">
				<div class="panel_icon">
					<i class="layui-icon" data-icon="&#xe63a;">&#xe63a;</i>
				</div>
				<div class="panel_word newMessage">
					<span><?php $result=mysql_query('select * from aikcms_gbook');echo mysql_num_rows($result);?></span>
					<cite>留言数量</cite>
				</div>
			</a>
		</div>
		<div class="panel col">
			<a href="javascript:;" data-url="page/user/user.php">
				<div class="panel_icon" style="background-color:#009688;">
					<i class="layui-icon" data-icon="&#xe613;">&#xe613;</i>
				</div>
				<div class="panel_word userAll">
					<span><?php $result=mysql_query('select * from aikcms_user');echo mysql_num_rows($result);?></span>
					<cite>用户总数</cite>
				</div>
			</a>
		</div>
		<div class="panel col">
			<a href="javascript:;" data-url="page/video/video_list.php">
				<div class="panel_icon" style="background-color:#5FB878;">
					<i class="layui-icon" data-icon="&#xe64a;">&#xe64a;</i>
				</div>
				<div class="panel_word imgAll">
					<span><?php $result=mysql_query('select * from aikcms_video');echo mysql_num_rows($result)+$num;?></span>
					<cite>视频总数</cite>
				</div>
			</a>
		</div>
		<div class="panel col max_panel">
			<a href="javascript:;" data-url="page/user/user_order.php">
				<div class="panel_icon" style="background-color:#2F4056;">
					<i class="iconfont icon-text" data-icon="icon-text"></i>
				</div>
				<div class="panel_word allNews">
					<span><?php $result=mysql_query('select * from aikcms_user_order');echo mysql_num_rows($result);?></span>
					<em>订单总数</em>
				</div>
			</a>
		</div>
	</div>
	<blockquote class="layui-elem-quote explain">
		<p>本后台基于layuiCms实现。<span style="color:#1E9FFF;">网络爱好者--></span><a href="http://www.domain.com target="_blank" class="layui-btn layui-btn-mini layui-btn-danger">点我打开</a><a href="http://www.mzahz.cn" target="_blank" class="layui-btn layui-btn-mini layui-btn-danger">免费秒赞</a>　</p>
		<p>更多资源尽在 <a target="_blank" href="http://www.domain.com" class="layui-btn layui-btn-mini layui-btn-danger">网络爱好者</a></p>
		<p><span style="color:#f00;">注：本CMS仅作为学习交流使用，切勿用作商业用途，谢谢！</span></p>
	</blockquote>
	<div class="row">
		<div class="sysNotice col">
			<blockquote class="layui-elem-quote title">系统基本参数</blockquote>
			<table class="layui-table">
				<colgroup>
					<col width="150">
					<col>
				</colgroup>
				<tbody>
					<tr>
						<td>当前版本</td>
						<td class="version">AikCms V2.0.1</td>
					</tr>
					<tr>
						<td>服务器操作系统：</td>
						<td class="author"><?php $os = explode(" ", php_uname()); echo $os[0];?> (<?php if('/'==DIRECTORY_SEPARATOR){echo $os[2];}else{echo $os[1];} ?>)</td>
					</tr>
					<tr>
						<td>服务器解译引擎：</td>
						<td class="homePage"><?php echo $_SERVER['SERVER_SOFTWARE'];?></td>
					</tr>
					<tr>
						<td>PHP版本：</td>
						<td class="server"><?php echo PHP_VERSION?></td>
					</tr>
					<tr>
						<td>域名：</td>
						<td class="dataBase"><?php echo $_SERVER['HTTP_HOST']?></td>
					</tr>
					<tr>
						<td>curl_init：</td>
						<td class="maxUpload"><?php if ( function_exists('curl_init') ){ echo '<span class="green">支持</span>' ;}else{ echo '<span class="red">不支持</span>';}?></td>
					</tr>
				</tbody>
			</table> 
		</div>	
		<div class="sysNotice col">
			<blockquote class="layui-elem-quote title">最新源码<i class="iconfont icon-new1"></i></blockquote>
			<table class="layui-table" lay-skin="line">
				<colgroup>
					<col>
					<col width="110">
				</colgroup>
				<tbody class="hot_news"> <?php $i=0;foreach($resultnm as $k=>$c){ if ($i<10){?>                     
<tr>
    <td align="left"><a href="<?php echo $c['link']?>" target="_blank"><?php echo $c['title']?></a></td>
</tr>
 <?php $i ++;}}?></tbody>
			</table> 
		</div>
	</div>

	<script type="text/javascript" src="../layui/layui.js"></script>
	<script type="text/javascript" src="../js/main.js"></script>
</body>
</html>
