<?php
include '../../../common/basic.php';
include '../admincore/video_add.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>发布视频</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui_video.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody layui-form">
        <blockquote class="layui-elem-quote">
		 <div class="layui-form-item"  style="margin-bottom:0px"><form method="post">
          <div class="layui-inline">	
		  <label class="layui-form-label">采集规则</label>
				<div class="layui-input-inline">  
						<select name="aik_col_rule" class="newsLook" lay-filter="browseLook">
					<?php
							$result = mysql_query('select * from aikcms_colgroup');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['aik_colgroup_fburl']?>"><?php echo $row['aik_colgroup_name']?></option>
                     <?php }?>
				    </select>
					</div>
			</div>
			<div class="layui-inline"  style="width:100%;">		
				<label class="layui-form-label">资源站链接</label>
				<div class="layui-input-inline" style="width:40%;">
					<input class="layui-input newsAuthor" name="aik_zylink" placeholder="填写资源站链接，获取视频内容！如直接发布请留空！" type="text">
				</div><button class="layui-btn layui-btn-normal" type="submit" name="get" >点击获取</button>
			</div>	</form>
		</div>
	</blockquote>
 	
		<form method="post" enctype="multipart/form-data" >
		<div class="layui-form-item">
			<label class="layui-form-label">视频标题</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_video_name" placeholder="请输入视频标题" type="text" value='<?php echo $vname;?>' >
			</div>
		</div>
		<input name="aik_video_zylink" type="hidden" value='<?php echo $aik_zylink;?>' >
		<input name="aik_video_col" type="hidden" value='<?php echo $cjgz;?>' >
	<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">自定义SEO</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_video_seo" placeholder="请输入视频相关关键词" type="text" >
			</div>
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">视频备注</label>
				<div class="layui-input-inline">
					<input class="layui-input newsAuthor" name="aik_video_remarks" placeholder="视频备注" type="text" value='<?php echo $remarks;?>'>
				</div>
			</div>
		</div>	
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">视频海报</label>
				<div class="layui-inline">
		    <div class="layui-input-inline">
		    	<input  name="aik_video_img" placeholder="这里可以输入网络图片" class="layui-input search_input"  style="width:102%;" value="<?php echo $vimg;?>">
              </div>  
                <input type="file" name="aik_video_img"  style="height:38px;"  />
		       </div> 
			</div>
		</div>
			<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">视频分类</label>
				<div class="layui-input-inline">
					<select name="aik_video_group" class="newsLook" lay-filter="browseLook">
					<?php
							$result = mysql_query('select * from aikcms_videogroup');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['id']?>"><?php echo $row['aik_videogroup_name']?></option>
                     <?php }?>
				    </select>
				</div>
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">观看积分</label>
				<div class="layui-input-inline">
					<input class="layui-input newsAuthor" name="aik_video_int" placeholder="请输入购买积分" type="text" value="0">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">观看用户组</label>
				<div class="layui-input-block">
				<input name="usergroup[]" class="tuijian" value="0"  title="无需会员" checked type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>无需会员</span><i class="layui-icon"></i></div>
				<?php
							$result = mysql_query('select * from aikcms_usergroup');
							while($row = mysql_fetch_array($result)){
						?>
					<input name="usergroup[]" class="tuijian" value="<?php echo $row['id']?>"  title="<?php echo $row['aik_usergroup_name']?>" type="checkbox"><div class="layui-unselect layui-form-checkbox"><span><?php echo $row['aik_usergroup_name']?></span><i class="layui-icon"></i></div>
					 <?php }?>
				</div>
			</div>
		</div>

		<div class="layui-form-item">
				<label class="layui-form-label">推荐级别</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_video_level" placeholder="请输入数字" type="text" value="0">
				</div><p style="color:red; float:left;line-height:38px;">*数字越高，排序越靠前！</p>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">解析设置</label>
				<div class="layui-input-inline">
					<select name="aik_video_jiexi" class="newsLook" lay-filter="browseLook">
					<option value="">无需解析</option>
					<?php
							$result = mysql_query('select * from aikcms_basic');
							while($row = mysql_fetch_array($result)){
						 if(!empty($row['aik_fbjiexi1'])){echo '<option value="'.$row['aik_fbjiexi1'].'">解析线路一</option>';}
						 if(!empty($row['aik_fbjiexi2'])){echo '<option value="'.$row['aik_fbjiexi2'].'">解析线路二</option>';}
					 }?>
				    </select>
			</div>
		</div>
		</div>
		<div class="layui-form-item layui-form-text">
				    <label class="layui-form-label">视频链接</label>
				    <div class="layui-input-block">
				      <textarea name="aik_video_url" placeholder="请输入视频链接" class="layui-textarea"><?php echo $alllink;?></textarea>
				    <p style="color:red; float:left;line-height:38px;">名称$链接</p>
				  </div></div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save">立即发布</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div><br>
	</form>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>