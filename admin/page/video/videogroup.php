<?php
include('../../../common/basic.php');
include('../admincore/videogroup.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>视频分类管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui_video.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
		<blockquote class="layui-elem-quote title">添加分组</blockquote><br>
		<form method="post" >
		 <div class="layui-form">
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">视频分组名称</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_videogroup_name" placeholder="视频分组名称" type="text">
				</div>
			</div>
		</div><div class="layui-form-item">
				<div class="layui-inline">
				<label class="layui-form-label">观看会员组</label>
				<div class="layui-input-block">
				<input name="usergroup[]" class="tuijian" value="0"  title="无需会员" checked type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>无需会员</span><i class="layui-icon"></i></div>
				<?php
							$result = mysql_query('select * from aikcms_usergroup');
							while($row = mysql_fetch_array($result)){
						?>
					<input name="usergroup[]" class="tuijian" value="<?php echo $row['id']?>"  title="<?php echo $row['aik_usergroup_name']?>" type="checkbox"><div class="layui-unselect layui-form-checkbox"><span><?php echo $row['aik_usergroup_name']?></span><i class="layui-icon"></i></div>
					 <?php }?>
				</div>
			</div>
		</div>
		<div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn layui-btn-normal" name="save">点击添加</button>
				    </div>
				  </div>
		</div>
		</form> 
		        <blockquote class="layui-elem-quote title">视频分类</blockquote>
                    <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col width="200">
					</colgroup>
					<thead>
						<tr>
						    <th>ID</th>
							<th style="text-align:left;">视频分类</th>
							<th>观看用户组</th>
							<th>视频数量</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$result = mysql_query('select * from aikcms_videogroup order by id desc');
						while($row = mysql_fetch_array($result)){
						?>
						<tr>
						    <td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_videogroup_name'];?></td>
							<td><?php echo aik_video_usergroup($row['aik_videogroup_usergroup']);if(in_array("0", explode(",", $row['aik_videogroup_usergroup']))){echo '无需会员';}?></td>
							<td><?php echo aik_video_num($row['id']);?></td>
							<td>
								<a class="layui-btn layui-btn-mini"  href="videogroup_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>
			    </div>
			     <!-- 登录日志 -->
		    </div>
		</div>

</section>

<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>