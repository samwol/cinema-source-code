<?php
include('../../../common/basic.php');
include('../admincore/video_edit.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>视频编辑</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui_video.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody layui-form">
	<?php
					$result = mysql_query('select * from aikcms_video where id = '.$_GET['id'].' ');	
					if ($row = mysql_fetch_array($result)){
					?>
		<form method="post" enctype="multipart/form-data" >
		<div class="layui-form-item">
			<label class="layui-form-label">视频标题</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_video_name" placeholder="请输入视频标题" type="text" value="<?php echo $row['aik_video_name'];?>" >
			</div>
		</div>
	<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">自定义SEO</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_video_seo" placeholder="请输入视频相关关键词" type="text" value="<?php echo $row['aik_video_seo'];?>" >
			</div>
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">视频备注</label>
				<div class="layui-input-inline">
					<input class="layui-input newsAuthor" name="aik_video_remarks" placeholder="视频备注" type="text" value="<?php echo $row['aik_video_remarks'];?>">
				</div>
			</div>
		</div>	
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">视频海报</label>
				<div class="layui-inline">
		    <div class="layui-input-inline">
		    	<input  name="aik_video_img" placeholder="这里可以输入网络图片" class="layui-input search_input"  style="width:102%;" value="<?php echo $row['aik_video_img'];?>">
              </div>  
                <input type="file" name="aik_video_img"  style="height:38px;"  />
		</div> 
			</div>
		</div>
		<div class="layui-form-item">
		<div class="layui-inline">		
				<label class="layui-form-label">视频分类</label>
				<div class="layui-input-inline">
					<select name="aik_video_group" class="newsLook" lay-filter="browseLook">
					<?php echo videogroup_select_list($row['aik_video_group']); ?>
				    </select>
				</div>
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">观看积分</label>
				<div class="layui-input-inline">
					<input class="layui-input newsAuthor" name="aik_video_int" placeholder="请输入购买积分" type="text" value="<?php echo $row['aik_video_int'];?>">
				</div>
			</div>
		</div>
        <div class="layui-form-item">
			<div class="layui-inline">
				<label class="layui-form-label">观看用户组</label>
				<div class="layui-input-block">
				<input name="usergroup[]" class="tuijian" value="0"  title="无需会员" <?php echo $a = in_array("0", explode(",", $row['aik_video_usergroup'])) ? 'checked' : '';?>  type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>无需会员</span><i class="layui-icon"></i></div>
			     <?php  echo aik_videoselect_usergroup($row['aik_video_usergroup']);?>
				</div>
			</div>
		</div>
		<div class="layui-form-item">
				<label class="layui-form-label">推荐级别</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_video_level" placeholder="请输入数字" type="text" value="<?php echo $row['aik_video_level'];?>">
				</div><p style="color:red; float:left;line-height:38px;">*数字越高，排序越靠前！</p>
		</div>
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">解析设置</label>
				<div class="layui-input-inline">
					<select name="aik_video_jiexi" class="newsLook" lay-filter="browseLook">
					<option value="0">无需解析</option>
					<?php
							$resultt = mysql_query('select * from aikcms_basic');
							while($roww = mysql_fetch_array($resultt)){
						 if(!empty($roww['aik_fbjiexi1'])){echo '<option value="'.$roww['aik_fbjiexi1'].'">解析线路一</option>';}
						 if(!empty($roww['aik_fbjiexi2'])){echo '<option value="'.$roww['aik_fbjiexi2'].'">解析线路二</option>';}
					 }?>
				    </select>
			</div>
		</div>
		</div>
		<div class="layui-form-item layui-form-text">
				    <label class="layui-form-label">视频链接</label>
				    <div class="layui-input-block">
				      <textarea name="aik_video_url" placeholder="请输入视频链接" class="layui-textarea"><?php echo $row['aik_video_url'];?></textarea>
				    <p style="color:red; float:left;line-height:38px;">名称$链接</p>
				  </div></div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="update">立即更新</button>
		    </div>
		</div><br>
	</form><?php }?>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>