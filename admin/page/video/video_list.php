<?php
include('../../../common/basic.php');
include('../admincore/video_list.php');
?>
<script type="text/javascript">
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>视频列表</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal"><form method="post">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
		<div class="layui-inline">
		    <div class="layui-input-inline" style="width:300px;">
		    	<input  name="wd" placeholder="请输入要搜索的视频标题" class="layui-input search_input" type="text" >
		    </div><button id="search" class="layui-btn" type="submit" >查询</button>
		</div><div class="layui-inline"><a class="layui-btn layui-btn-normal" href="video_add.php">发布视频</a></div>
			  <div class="layui-inline"><input type="submit" id="update" class="layui-btn recommend" name="update"  value="批量更新" /></div>
			  <div class="layui-inline"><input type="submit" id="execute" class="layui-btn layui-btn-danger" name="execute" onclick="return confirm('确定要进行删除吗')" value="批量删除" /></div>	
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col width="80">
						<col>
						<col width="100">
						<col>
						<col>
						<col>
						<col>
						<col>
					</colgroup>
					<thead>
						<tr>
							<th><input name="" lay-filter="allChoose" id="allChoose" type="checkbox">
								<div class="layui-unselect layui-form-checkbox" ><i class="layui-icon"></i></div>
							</th>
							<th>ID</th>
							<th style="text-align:left;">视频标题</th>
							<th>视频分类</th>
							<th>所需用户组</th>
							<th>视频海报</th>
							<th>购买积分</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
					<?php

									$sql = 'select * from aikcms_video order by id desc';
									$pager = page_handle('page',20,mysql_num_rows(mysql_query($sql)));
									$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
				
							if (isset($_GET['wd'])) {
								$sql = 'select * from aikcms_video where aik_video_name like "%'.$_GET['wd'].'%" order by id desc';
								$pager = page_handle('page',20,mysql_num_rows(mysql_query($sql)));
								$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							}
							while($row= mysql_fetch_array($result)){
							?>
						<tr>
							<td><input name="id[]" type="checkbox" value="<?php echo $row['id'] ?>" />
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</td>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_video_name'];?><?php if(!empty($row['aik_video_remarks'])){ echo '<font color="#FF5722">['.$row['aik_video_remarks'].']';}?></font></td>
							<td><?php echo get_videogroup_name($row['aik_video_group'])?></td>
							<td><?php echo aik_video_usergroup($row['aik_video_usergroup']);if(in_array("0", explode(",", $row['aik_video_usergroup']))){echo '无需会员';}?></td>
							<td style="padding: 0 15px;"><img layer-src="<?php echo $row['aik_video_img'];?>" src="<?php echo $row['aik_video_img'];?>" style="height:41px;"></td>
							<td><?php echo $a = $row['aik_video_int']=='0' ? '免费':$row['aik_video_int'];?></td>
							<td>
							    <?php if(!empty($row['aik_video_zylink'])){echo '<a class="layui-btn layui-btn-normal layui-btn-mini" href="?updateimg='.$row['id'].'&cjgz='.$row['aik_video_col'].'">点击更新</a>';}?>
								<a class="layui-btn layui-btn-mini" href="video_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						
						<?php } ?>
					</tbody>	
					</table>
			<div class="layui-form-item">
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div>
			     <!-- 登录日志 -->
			    
		    </div>
		</div></form>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>