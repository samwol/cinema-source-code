<?php
include('../../../common/basic.php');
include('../admincore/zhibo.php');
?>
<script type="text/javascript">
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>直播管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
<?php
$result = mysql_query('select * from aikcms_basic where id = 1');
					if( $row = mysql_fetch_array($result)){
					?>		
       <form method="post"> <blockquote class="layui-elem-quote news_search">
	<div class="layui-form" >
				  <div class="layui-form-item" style="margin-bottom: 0;">
				    <label class="layui-form-label">直播来源</label>
					
				    <div class="layui-input-block">
				      <input type="radio" name="aik_zhibo" value="1" title="调用代码" <?php echo $row['aik_zhibo'] == 1 ? 'checked' : '';?>>
				      <input type="radio" name="aik_zhibo" value="2" title="自己发布" <?php echo $row['aik_zhibo'] == 2 ? 'checked' : '';?>>
				   
				      <button class="layui-btn" name="update">立即提交</button>
				    </div>
				  </div>
				  </div>
		
	</blockquote> 
					<?php }?>
	
	<blockquote class="layui-elem-quote news_search">
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal zhiboAdd_btn">添加直播源</a>
			<input type="submit" id="execute" class="layui-btn layui-btn-danger" name="execute" onclick="return confirm('确定要进行操作吗')" value="批量删除" />
		</div>
		
	</blockquote>        
		         <!-- 操作日志 -->
				 
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col width="50">
						<col>
						<col>
						<col width="200">
					</colgroup>
					<thead>
						<tr>
						    <th><input name="" lay-filter="allChoose" id="allChoose" type="checkbox">
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</th>
						    <th>ID</th>
							<th style="text-align:left;">直播名称</th>
							<th>直播链接</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$sql = 'select * from aikcms_zhibo order by id desc';
						$pager = page_handle('page',10,mysql_num_rows(mysql_query($sql)));
						$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
						while($row= mysql_fetch_array($result)){
						?>
						<tr>
							<td><input name="id[]" type="checkbox" value="<?php echo $row['id'] ?>" />
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</td>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_zhibo_name'];?></td>
							<td><?php echo $row['aik_zhibo_url'];?></td>
							<td>
								<a class="layui-btn layui-btn-mini"  href="zhibo_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>" onclick="return confirm('确认要删除吗？')"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>  <div class="layui-form-item">
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div>
			    </div></form>
			     <!-- 登录日志 -->
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>