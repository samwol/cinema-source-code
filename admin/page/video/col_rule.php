<?php
include('../../../common/basic.php');
include('../admincore/col_rule.php');
?>
<script type="text/javascript">
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>采集管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
		<form method="post">
        <div class="layui-inline">
			<a class="layui-btn layui-btn-normal col_ruleAdd_btn">点击添加采集</a>
			<a class="layui-btn layui-btn-normal col_ruleurlAdd_btn" style="background-color:#5FB878">点击添加采集规则</a>
			<input type="submit" id="execute" class="layui-btn layui-btn-danger" name="execute" onclick="return confirm('确定要进行操作吗')" value="批量删除" />
		</div>
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col width="250">
					</colgroup>
					<thead>
						<tr>
							<th><input name=""  lay-filter="allChoose" id="allChoose" type="checkbox">
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</th>
							<th>ID</th>
							<th style="text-align:left;">采集名称</th>
							<th>视频分类</th>
							<th>观看用户组</th>
							<th>开始页码</th>
							<th>结束页码</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
				<?php

									$sql = 'select * from aikcms_colrule order by id desc';
									$pager = page_handle('page',10,mysql_num_rows(mysql_query($sql)));
									$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							while($row= mysql_fetch_array($result)){
							?>
						<tr>
							<td><input name="id[]" type="checkbox" value="<?php echo $row['id'] ?>" />
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</td>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_col_name'];?></td>
							<td><?php echo get_videogroup_name($row['aik_col_videogroup']);?></td>
							<td><?php echo aik_video_usergroup($row['aik_col_usergroup']);if(in_array("0", explode(",", $row['aik_col_usergroup']))){echo '无需会员';}?></td>
							<td><?php echo $row['aik_col_urlstart'];?></td>
							<td><?php echo $row['aik_col_urlend'];?></td> 
							<td>
							<a class="layui-btn layui-btn-normal layui-btn-mini" href="?caijiid=<?php echo $row['id'] ?>&cid=<?php echo $row['aik_col_urlstart']?>&cjgz=<?php echo $row['aik_col_rule']?>">点击采集</a>
								<a class="layui-btn layui-btn-mini" href="col_rule_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini"  href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						
						<?php } ?>
					</tbody>	
					</table>
			<div class="layui-form-item">
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div></form>
			     <!-- 登录日志 -->
			    
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>