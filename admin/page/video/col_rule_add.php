<?php
include('../../../common/basic.php');
include('../admincore/col_rule_add.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>添加采集</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui_video.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">


<section class="layui-larry-box">
		<div class="larry-personal-body clearfix layui-form">
		
			<form class="layui-form col-lg-6" method="post" >
			
			<div class="layui-form-item">	
				<label class="layui-form-label">采集名称</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_col_name"  autocomplete="off"  class="layui-input" placeholder="用于区分各采集" >
					</div> 
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">采集网址</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_col_url"  autocomplete="off"  class="layui-input"  placeholder="输入采集用的网址">
					</div> 
		</div>
			<div class="layui-form-item">
            <div class="layui-inline">		
				<label class="layui-form-label">开始页码</label>
				<div class="layui-input-inline">
					<input type="text" name="aik_col_urlstart"  autocomplete="off"  class="layui-input"  value="1">
				</div>
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">结束页码</label>
				<div class="layui-input-inline">
				<input type="text" name="aik_col_urlend"  autocomplete="off"  class="layui-input"  placeholder="请输入采集结束页码">
			</div>
		</div>
		</div>	<div class="layui-form-item">
            <div class="layui-inline">		
				<label class="layui-form-label">视频分类</label>
				<div class="layui-input-inline">
					<select name="aik_col_videogroup" class="newsLook" lay-filter="browseLook">
					<?php
							$result = mysql_query('select * from aikcms_videogroup');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['id']?>"><?php echo $row['aik_videogroup_name']?></option>
                     <?php }?>
				    </select>
				</div>
			</div>
				<div class="layui-inline">
				<label class="layui-form-label">观看会员组</label>
				<div class="layui-input-block">
				<input name="usergroup[]" class="tuijian" value="0"  title="无需会员" checked type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>无需会员</span><i class="layui-icon"></i></div>
				<?php
							$result = mysql_query('select * from aikcms_usergroup');
							while($row = mysql_fetch_array($result)){
						?>
					<input name="usergroup[]" class="tuijian" value="<?php echo $row['id']?>"  title="<?php echo $row['aik_usergroup_name']?>" type="checkbox"><div class="layui-unselect layui-form-checkbox"><span><?php echo $row['aik_usergroup_name']?></span><i class="layui-icon"></i></div>
					 <?php }?>
				</div>
			</div>
		</div>	
		<div class="layui-form-item">	
				<label class="layui-form-label">采集规则</label>
				<div class="layui-input-block">  
						<select name="aik_col_rule" class="newsLook" lay-filter="browseLook">
					<?php
							$result = mysql_query('select * from aikcms_colgroup');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['aik_colgroup_cjurl']?>"><?php echo $row['aik_colgroup_name']?></option>
                     <?php }?>
				    </select>
					</div> 
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save" >立即添加</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
			</form>
					</div>

</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>