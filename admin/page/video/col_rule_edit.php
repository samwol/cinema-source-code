<?php
include('../../../common/basic.php');
include('../admincore/col_rule_edit.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>添加采集规则</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui_video.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">	
<section class="layui-larry-box">
		<div class="larry-personal-body clearfix layui-form">
		<?php
					$result = mysql_query('select * from aikcms_colrule where id = '.$_GET['id'].' ');	
					if ($row = mysql_fetch_array($result)){
					?>
			<form class="layui-form col-lg-6" method="post" >
			
			<div class="layui-form-item">	
				<label class="layui-form-label">采集名称</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_col_name"  autocomplete="off"  class="layui-input"  value="<?php echo $row['aik_col_name'];?>" >
					</div> 
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">采集网址</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_col_url"  autocomplete="off"  class="layui-input"  value="<?php echo $row['aik_col_url'];?>">
					</div> 
		</div>
			<div class="layui-form-item">
            <div class="layui-inline">		
				<label class="layui-form-label">开始页码</label>
				<div class="layui-input-inline">
					<input type="text" name="aik_col_urlstart"  autocomplete="off"  class="layui-input"  value="<?php echo $row['aik_col_urlstart'];?>">
				</div>
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">结束页码</label>
				<div class="layui-input-inline">
				<input type="text" name="aik_col_urlend"  autocomplete="off"  class="layui-input"  value="<?php echo $row['aik_col_urlend'];?>">
			</div>
		</div>
		</div>	<div class="layui-form-item">
            <div class="layui-inline">		
				<label class="layui-form-label">视频分类</label>
				<div class="layui-input-inline">
					<select name="aik_col_videogroup" class="newsLook" lay-filter="browseLook">
					<?php echo videogroup_select_list($row['aik_col_videogroup']); ?>
				    </select>
				</div>
			</div>
		<div class="layui-inline">
				<label class="layui-form-label">观看会员组</label>
				<div class="layui-input-block">
				<input name="usergroup[]" class="tuijian" value="0"  title="无需会员" <?php echo $a = in_array("0", explode(",", $row['aik_col_usergroup'])) ? 'checked' : '';?> type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>无需会员</span><i class="layui-icon"></i></div>
				<?php  echo aik_videoselect_usergroup($row['aik_col_usergroup']);?>
				</div>
			</div>
		</div>	
		<div class="layui-form-item">	
				<label class="layui-form-label">采集规则</label>
				<div class="layui-input-block">  
						<select name="aik_col_rule" class="newsLook" lay-filter="browseLook">
					<?php  echo colrule_select_list($row['aik_col_rule']);?>
				    </select>
					</div> 
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="update" >立即更新</button>
		    </div>
		</div></br>
			</form><?php }?>
					</div>

</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>