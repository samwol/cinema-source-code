<?php
include('../../../common/basic.php');
include('../admincore/caiji_rule.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>采集规则管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui_video.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
		<blockquote class="layui-elem-quote title">添加采集规则</blockquote><br>
		<form method="post" enctype="multipart/form-data" >
		 <div class="layui-form">
		<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">采集规则名称</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_colgroup_name" placeholder="采集规则名称" type="text">
				</div>
			</div>
		</div>	<div class="layui-form-item">
			<div class="layui-inline">		
				<label class="layui-form-label">发布规则</label>
				<div class="layui-inline">
		    <div class="layui-input-inline">
		    	<input  name="aik_colgroup_fburl"  class="layui-input"  style="width:102%;">
              </div>  
                <input type="file" name="aik_colgroup_fburl"  style="height:38px;"  />
		</div> 
			</div>
			<div class="layui-inline">		
				<label class="layui-form-label">采集规则</label>
				<div class="layui-inline">
		    <div class="layui-input-inline">
		    	<input  name="aik_colgroup_cjurl" class="layui-input"  style="width:102%;">
              </div>  
                <input type="file" name="aik_colgroup_cjurl"  style="height:38px;"  />
		</div> 
			</div>
		</div>
		<div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn layui-btn-normal" name="save">点击添加</button>
				    </div>
				  </div>
		</div>
		</form> 
		        <blockquote class="layui-elem-quote title">视频分类</blockquote>
                    <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col width="100">
					</colgroup>
					<thead>
						<tr>
						    <th>ID</th>
							<th style="text-align:left;">采集规则名称</th>
							<th>发布规则文件</th>
							<th>采集规则文件</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$result = mysql_query('select * from aikcms_colgroup order by id desc');
						while($row = mysql_fetch_array($result)){
						?>
						<tr>
						    <td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_colgroup_name'];?></td>
							<td><?php echo $row['aik_colgroup_fburl'];?></td>
							<td><?php echo $row['aik_colgroup_cjurl'];?></td>
							<td>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>
			    </div>
			     <!-- 登录日志 -->
		    </div>
		</div>

</section>

<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>