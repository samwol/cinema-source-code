<?php
include('../../../common/basic.php');
include('../admincore/zhibo_add.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>添加直播源</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">
	<form class="layui-form" method="post">
		<div class="layui-form-item">
				<label class="layui-form-label">直播源名称</label>
				<div class="layui-input-inline">
					<input class="layui-input" name="aik_zhibo_name" placeholder="请输入直播源名称" type="text">
				</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">直播源链接</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_zhibo_url" placeholder="请输入直播源链接" type="text">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div><br>
	</form>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>