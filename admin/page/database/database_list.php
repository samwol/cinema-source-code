<?php
$conn=include('../../../common/config/conn.php');
include('../admincore/backdata.php');
?>
<script type="text/javascript">
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>数据备份</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal"><form method="post">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
        <div class="layui-inline">
			   <input type="submit" id="databack" class="layui-btn layui-btn-normal" name="databack"  value="备份数据库" />
			   <input type="submit" id="alloptim" class="layui-btn layui-btn-danger" name="alloptim"  value="全部优化" />
			  
		</div>
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col width="250">
						
					</colgroup>
					<thead>
						<tr>
							<th><input name="" lay-filter="allChoose" id="allChoose" type="checkbox">
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</th>
							<th style="text-align:left;">数据库名称</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
					<?php
mysql_select_db($conn['db_name']) or die("数据库连接失败！"); 
$result = mysql_query("SHOW TABLES"); 
while($row = mysql_fetch_array($result)) 
{ 

?>
						<tr>
							<td><input name="id[]" type="checkbox" value="<?php echo $row[0]; ?>" />
								<div class="layui-unselect layui-form-checkbox" lay-skin="primary"><i class="layui-icon"></i></div>
							</td>
							<td><?php echo $row[0]; ?></td>
							<td>
								<a class="layui-btn layui-btn-mini" href="?name=<?php echo $row[0];?>"> 点击备份 </a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?optim=<?php echo $row[0];?>">点击优化</a>
							</td>
						</tr>
					<?php	} 
mysql_free_result($result);
							?>
					</tbody>	
					</table>
			     <!-- 登录日志 -->
			    
		    </div>
		</div></form>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>