<?php
include('../../../common/basic.php');
include('../admincore/databack_list.php');
?>
<script type="text/javascript">
	function check_all(cname) {
	$('input[name="'+cname+'"]:checkbox').each(function(){
		this.checked = !this.checked;
	});
}
</script>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>数据还原</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal"><form method="post">
	    <div class="layui-tab" style="overflow:visible">
		         <!-- 操作日志 -->
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col>
						<col width="250">
						
					</colgroup>
					<thead>
						<tr>
							<th style="text-align:left;">已备份文件</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
					<?php
foreach(glob('../../../common/databack/*.sql') as $sql)
{
?>
						<tr>
							<td><?php echo basename($sql);?></td>
							<td>
							    <a class="layui-btn layui-btn-mini" href="?name=<?php echo basename($sql);?>"> 点击还原 </a>
								<a class="layui-btn layui-btn-normal layui-btn-mini" href="../../../common/databack/<?php echo basename($sql);?>"  download="<?php echo basename($sql);?>"> 点击下载 </a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo basename($sql);?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>	
					</table>
			     <!-- 登录日志 -->
			    
		    </div>
		</div></form>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>