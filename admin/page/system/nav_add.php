<?php
include('../../../common/basic.php');
include('../admincore/nav_add.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>添加导航</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
    <link rel="stylesheet" type="text/css" href="style/jquery.bigcolorpicker.css" />
    <script type="text/javascript" src="../../js/jquery.min.js" ></script>
    <script type="text/javascript" src="style/jquery.bigcolorpicker.min.js"></script>
	<script type="text/javascript">
$(function(){
	$("#color").bigColorpicker("color");
});
    </script>
</head>
<body class="childrenBody">
	<form class="layui-form" method="post">
		<div class="layui-form-item">	
		<div class="layui-inline">
			<label class="layui-form-label">导航名称</label>
			<div class="layui-input-inline">
				<input class="layui-input" name="aik_nav_name" lay-verify="required" placeholder="请输入导航名称" type="text">
			</div>
		</div>
		<div class="layui-inline">
			<label class="layui-form-label">颜色</label>
			<div class="layui-input-inline">
				<input class="layui-input" name="aik_nav_color" id="color"/>
			</div>
		</div>
		</div>
		<div class="layui-form-item">	
		<div class="layui-inline">
				<label class="layui-form-label">上级导航</label>
				<div class="layui-input-inline">
					<select name="aik_nav_papa" class="newsLook" lay-filter="browseLook">
				        <option value="0">一级导航</option>
						<?php
							$result = mysql_query('select * from aikcms_nav where aik_nav_papa="0"');
							while($row = mysql_fetch_array($result)){
						?>
						<option value="<?php echo $row['id']?>"><?php echo $row['aik_nav_name']?></option>
                     <?php }?>
				    </select>
				</div>
				</div>
			<div class="layui-inline">
			<label class="layui-form-label">排序</label>
				<div class="layui-input-inline">
				     <input class="layui-input" name="aik_nav_id"  placeholder="请输入数字" type="text" value="0">
				</div><div class="layui-form-mid layui-word-aux">数字越大越靠前！</div>
		</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">链接</label>
			<div class="layui-input-block">
				<input class="layui-input" name="aik_nav_url" placeholder="请输入导航链接" type="text">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save" >立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
	</form><blockquote class="layui-elem-quote"><p style="color:red; margin:0 auto;line-height:38px;">1.颜色留空为默认颜色！</p></blockquote><br>
<script type="text/javascript" src="../../layui/layui.js"></script>

</body>
</html>