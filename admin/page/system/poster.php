<?php
include('../../../common/basic.php');
include('../admincore/postercome.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>幻灯片设置</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
   <blockquote class="layui-elem-quote news_search">
		
<?php
$result = mysql_query('select * from aikcms_basic where id = 1');
					if( $row = mysql_fetch_array($result)){
					?>
		<form method="post"><div class="layui-form" >
				  <div class="layui-form-item" style="margin-bottom: 0;">
				    <label class="layui-form-label">幻灯片来源</label>
					
				    <div class="layui-input-block">
				      <input type="radio" name="aik_postercome" value="1" title="自动更新" <?php if( $row['aik_postercome'] == 1 ){ echo 'checked' ;}?> >
				      <input type="radio" name="aik_postercome" value="2" title="手动更新" <?php if( $row['aik_postercome'] == 2 ){ echo 'checked' ;}?>>
				      <input type="radio" name="aik_postercome" value="3" title="两者皆有" <?php if( $row['aik_postercome'] == 3 ){ echo 'checked' ;}?>>
				   
				      <button class="layui-btn" name="update">立即提交</button>
				    </div>
				  </div>
				  </div></form>
					<?php }?>
	</blockquote>
        <blockquote class="layui-elem-quote news_search">
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal newsAdd_btn">添加新幻灯片</a>
		</div>
		
	</blockquote>          
		         <!-- 操作日志 -->
				 
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col width="200">
					</colgroup>
					<thead>
						<tr>
						    <th>ID</th>
							<th style="text-align:left;">幻灯片标题</th>
							<th>图片</th>
							<th>链接</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$result = mysql_query('select * from aikcms_poster order by id desc');
						while($row = mysql_fetch_array($result)){
						?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_hd_name'];?></td>
							<td><img src="<?php echo $row['aik_hd_img'];?>" style="height:41px;"></td>
							<td><?php echo $row['aik_hd_link'];?></td>
							<td>
								<a class="layui-btn layui-btn-mini"  href="poster_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>
			    </div>
			     <!-- 登录日志 -->
		    </div>
		</div>
<blockquote class="layui-elem-quote"><p style="color:red; margin:0 auto;line-height:38px;">1.部分模板不带幻灯片模块，设置无效！</p></blockquote><br>
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>