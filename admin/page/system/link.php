<?php
include('../../../common/basic.php');
include('../admincore/link.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>友情链接</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
        <blockquote class="layui-elem-quote news_search">
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal linkAdd_btn">添加友情链接</a>
		</div>
		
	</blockquote>          
		         <!-- 操作日志 -->
				 
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col width="30%">
						<col width="20%">
						<col width="20%">
						<col width="9%">
					</colgroup>
					<thead>
						<tr>
						    <th>ID</th>
							<th style="text-align:left;">友链标题</th>
							<th>LOGO图片</th>
							<th>链接</th>
							<th>站长QQ</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$result = mysql_query('select * from aikcms_link order by id desc');
						while($row = mysql_fetch_array($result)){
						?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_link_name'];?></td>
							<td><?php echo $a= $row['aik_link_img']<>'' ? '<img src="'.$row['aik_link_img'].'" style="height:41px;">':'暂无';?></td>
							<td><?php echo $row['aik_link_url'];?></td>
							<td><?php echo $a= $row['aik_link_qq']<>'' ? $row['aik_link_qq']:'暂无';?></td>
							<td>
								<a class="layui-btn layui-btn-mini"  href="link_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>" onclick="return confirm('确认要删除吗？')"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>
			    </div>
			     <!-- 登录日志 -->
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>