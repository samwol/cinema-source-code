<?php
include('../../../common/basic.php');
include('../admincore/basic.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>基本设置</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">
	<div class="row"><?php
$result = mysql_query('select * from aikcms_basic where id = 1');
					if( $row = mysql_fetch_array($result)){
					?>
		<form method="post">
		<div class="sysNotice col">
			<blockquote class="layui-elem-quote title">基本设置</blockquote><br>
			<div class="layui-form">
				<div class="user_left">
			<div class="layui-form-item">
			    <label class="layui-form-label">网站名称</label>
			    <div class="layui-input-block">
			    	<input type="text" name="aik_name" placeholder="请输入网站名称" value="<?php echo $row['aik_name']?>" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">网站域名</label>
			    <div class="layui-input-inline">
			    	<input type="text"  name="aik_domain" placeholder="请输入网站域名" value="<?php echo $row['aik_domain']?>"  class="layui-input">
			    </div><div class="layui-form-mid layui-word-aux">如：http://v.xchwl.cn/,一定要以斜杠结尾，否则出错！</div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">网站标题</label>
			    <div class="layui-input-block">
			    	<input type="text" name="aik_title" value="<?php echo $row['aik_title']?>" placeholder="请输入网站标题" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">SEO关键字</label>
			    <div class="layui-input-block">
			    	<input type="text" name="aik_keywords" value="<?php echo $row['aik_keywords']?>" placeholder="请输入SEO关键字" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">网站描述</label>
			    <div class="layui-input-block">
			    	<textarea name="aik_desc" placeholder="请输入网站描述" class="layui-textarea"><?php echo $row['aik_desc']?></textarea>
			    </div>
			</div>
						<div class="layui-form-item">
			    <label class="layui-form-label">PC模板</label>
				   <div class="layui-input-block">
				      <select name="aik_template">
					  <option value="<?php echo $row['aik_template']?>"><?php echo $row['aik_template']?></option>
		  <?php
$handler = opendir('../../../template');
while( ($filename = readdir($handler)) !== false ) 
{
 if($filename != '.' && $filename != '..')
 {  
 echo  '<option value='.$filename.'>'.$filename.'</option>';

  }
}
closedir($handler);?>		
				        
				      </select>
				    </div>  
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">手机独立模板</label>
			    <div class="layui-input-inline">
			    	<input type="radio" name="aik_onlym" value="1" title="开" <?php echo $a = $row['aik_onlym'] == 1 ? 'checked':'' ;?>>
				    <input type="radio" name="aik_onlym" value="0" title="关" <?php echo $a = $row['aik_onlym'] == 0 ? 'checked':'' ;?>>
			    </div>
				<label class="layui-form-label">手机模板</label>
				   <div class="layui-input-inline">
				      <select name="aik_mtemplate">
					  <option value="<?php echo $row['aik_mtemplate']?>"><?php echo $row['aik_mtemplate']?></option>
		  <?php
$handler = opendir('../../../template');
while( ($filename = readdir($handler)) !== false ) 
{
 if($filename != '.' && $filename != '..')
 {  
 echo  '<option value='.$filename.'>'.$filename.'</option>';

  }
}
closedir($handler);?>		
				        
				      </select>
				    </div>
			</div>	
			<div class="layui-form-item">
			    <label class="layui-form-label">ICP备案号</label>
			    <div class="layui-input-inline">
			    	<input type="text" value="<?php echo $row['aik_icp']?>" name="aik_icp"  placeholder="请输入ICP备案号" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">统计代码</label>
				    <div class="layui-input-block">
				      <textarea name="aik_tongji" placeholder="请输入统计代码" class="layui-textarea"><?php echo $row['aik_tongji']?></textarea>
			</div></div>
			<div class="layui-form-item">
			     <label class="layui-form-label">畅言代码</label>
				    <div class="layui-input-block">
				      <textarea name="aik_changyan" placeholder="请输入畅言代码" class="layui-textarea"><?php echo $row['aik_changyan']?></textarea>
				    </div>
			</div>
		</div>
			</div>
		</div>
		<div class="sysNotice col">
			<blockquote class="layui-elem-quote title">解析线路</blockquote><br>
			<div class="layui-form-item">
				    <label class="layui-form-label">解析线路一</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_jiexi1" placeholder="请输入解析线路"  class="layui-input" value="<?php echo $row['aik_jiexi1']?>">
				    </div>
				  </div>
				   <div class="layui-form-item">
				    <label class="layui-form-label">解析线路二</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_jiexi2"  placeholder="请输入解析线路"  class="layui-input" value="<?php echo $row['aik_jiexi2']?>">
				    </div>
				  </div>
				   <div class="layui-form-item">
				    <label class="layui-form-label">解析线路三</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_jiexi3"  placeholder="请输入解析线路"  class="layui-input" value="<?php echo $row['aik_jiexi3']?>">
				    </div>
				  </div>
				   <div class="layui-form-item">
				    <label class="layui-form-label">解析线路四</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_jiexi4"  placeholder="请输入解析线路"  class="layui-input" value="<?php echo $row['aik_jiexi4']?>">
				    </div>
				  </div>
				   <div class="layui-form-item">
				    <label class="layui-form-label">解析线路五</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_jiexi5"  placeholder="请输入解析线路"  class="layui-input" value="<?php echo $row['aik_jiexi5']?>">
				    </div>
				  </div> 
				  <div class="layui-form-item">
				    <label class="layui-form-label">解析线路六</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_jiexi6"  placeholder="此线路必须支持M3U8解析"  class="layui-input" value="<?php echo $row['aik_jiexi6']?>">
				    </div>
				  </div> 
                  <div class="layui-form-item">
				    <label class="layui-form-label">调用解析一</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_fbjiexi1"  placeholder="视频发布页解析调用，可重复，如不用可留空"  class="layui-input" value="<?php echo $row['aik_fbjiexi1']?>">
				    </div>
				  </div>
				  <div class="layui-form-item">
				    <label class="layui-form-label">调用解析二</label>
				    <div class="layui-input-block">
				      <input type="text" name="aik_fbjiexi2"  placeholder="视频发布页解析调用，可重复，如不用可留空"  class="layui-input" value="<?php echo $row['aik_fbjiexi2']?>">
				    </div>
				  </div>
	
		    <blockquote class="layui-elem-quote title">其他设置</blockquote><br>
			 <div class="layui-form">
			 <div class="layui-form-item">
				    <label class="layui-form-label">全站缓存</label>
				    <div class="layui-input-block">
				      <input type="radio" name="aik_cache" value="1" title="开" <?php if( $row['aik_cache'] == 1 ){ echo 'checked' ;}?>>
				      <input type="radio" name="aik_cache" value="0" title="关" <?php if( $row['aik_cache'] == 0 ){ echo 'checked' ;}?>>
				    </div>
				  </div>
				   <div class="layui-form-item">
				    <label class="layui-form-label">伪静态</label>
				    <div class="layui-input-block">
				      <input type="radio" name="aik_weijing" value="1" title="开" <?php if( $row['aik_weijing'] == 1 ){ echo 'checked' ;}?>>
				      <input type="radio" name="aik_weijing" value="0" title="关" <?php if( $row['aik_weijing'] == 0 ){ echo 'checked' ;}?>>
				    </div>
				  </div> 
				  <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" name="update">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
				  </div>
				  </div>
		</div></form>
		<?php }?>
	</div>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>
