<?php
include('../../../common/basic.php');
include('../admincore/admin.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>管理员设置</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
	<link rel="stylesheet" href="../../css/user.css" media="all" />
	<script type="text/javascript" src="../../js/jquery.min.js" ></script>
</head>
<body class="childrenBody">


<section class="layui-larry-box">
	<div class="user_left">
		<?php
$result = mysql_query('select * from aikcms_basic where id = 1');
					if( $row = mysql_fetch_array($result)){
					?>
		<div class="larry-personal-body clearfix changepwd">
		
			<form class="layui-form" method="post" enctype="multipart/form-data">
			<div class="layui-form-item">
					<label class="layui-form-label">用户头像</label>
					<div class="layui-input-block">
						<div class="layui-box layui-upload-button"><input type="file" name="aik_admin_img" accept="image/*" class="layui-upload-file"  onchange="changImg(event)"><span class="layui-upload-icon"><i class="layui-icon"></i>上传图片</span></div>
					</div>
					<div   class="layui-input-block" style="height:100px; width:100px; border-radius:50%; overflow:hidden;border: 1px solid #e6e6e6;"> 
                        <input type="hidden" name="aik_admin_img"  value="<?php echo $row['aik_admin_img']?>"> 					
						<img  id="myImg"  alt="暂无图片" src="<?php echo $row['aik_admin_img'];?>"  style="height:100%; width:100%;border:0; line-height:100px;text-align:center;">
					</div>
					
				</div>
			<div class="layui-form-item">	
				<label class="layui-form-label">用户名</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_admin" placeholder="请输入用户名"  class="layui-input" value="<?php echo $row['aik_admin']?>">
					</div> 
		</div>
		 <input type="hidden" name="aik_pw"  value="<?php echo $row['aik_pw']?>"> 		
		<div class="layui-form-item">	
				<label class="layui-form-label">新密码</label>
<div class="layui-input-block">  
						<input type="password" name="aik_pw"  placeholder="请输入密码"  class="layui-input" >
					</div> 	
		</div><div class="layui-form-item">	
				<label class="layui-form-label">确认密码</label>
				<div class="layui-input-block">  
						<input type="password" name="aik_rpw"  placeholder="请重复密码"  class="layui-input">
					</div> 
		</div><div class="layui-form-item">	
				<label class="layui-form-label">管理员QQ</label>
				<div class="layui-input-block">  
						<input type="text" name="aik_admin_qq"  placeholder="请输入你的QQ"  class="layui-input" value="<?php echo $row['aik_admin_qq']?>">
					</div> 
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="save" >立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
			</form>
					</div><?php }?>
	</div>
</section>
<script type="text/javascript">  
     function changImg(e){  
        for (var i = 0; i < e.target.files.length; i++) {  
            var file = e.target.files.item(i);  
            if (!(/^image\/.*$/i.test(file.type))) {  
                continue; //不是图片 就跳出这一次循环  
            }  
            //实例化FileReader API  
            var freader = new FileReader();  
            freader.readAsDataURL(file);  
            freader.onload = function(e) {  
                $("#myImg").attr("src",e.target.result);  
            }  
        }  
    }  
</script>  
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>