<?php
include('../../../common/basic.php');
include('../admincore/nav.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>导航管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab">
   <blockquote class="layui-elem-quote">
		
<?php
$result = mysql_query('select * from aikcms_basic where id = 1');
					if( $row = mysql_fetch_array($result)){
					?>
		<form method="post"  enctype="multipart/form-data"><div class="layui-form" >
				  <div class="layui-form-item">
				    <label class="layui-form-label">导航LOGO</label>
				    <div class="layui-input-block">
				     <div class="layui-input-inline">
		    	<input  name="aik_biglogo" placeholder="这里可以输入网络图片" class="layui-input search_input"  style="width:102%;" value="<?php echo $row['aik_biglogo']?>">
                     </div>  
                   <input type="file" name="aik_biglogo"  style="height:38px;"  />
				    </div>
				  </div>
				  <div class="layui-form-item">
				    <label class="layui-form-label">备用Ⅰ</label>
				    <div class="layui-input-block">
				     <div class="layui-input-inline">
		    	<input  name="aik_onelogo" placeholder="这里可以输入网络图片" class="layui-input search_input"  style="width:102%;" value="<?php echo $row['aik_onelogo']?>">
                     </div>  
                   <input type="file" name="aik_onelogo"  style="height:38px;"  />
				    </div>
				  </div>
				  <div class="layui-form-item">
				    <label class="layui-form-label">备用Ⅱ</label>
				    <div class="layui-input-block">
				     <div class="layui-input-inline">
		    	<input  name="aik_twologo" placeholder="这里可以输入网络图片" class="layui-input search_input"  style="width:102%;" value="<?php echo $row['aik_twologo']?>">
                     </div>  
                   <input type="file" name="aik_twologo"  style="height:38px;"  />
				    </div>
				  </div>
				  <div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" name="update" >立即提交</button>
		    </div>
		</div>
				  </div>
				  </form>
					<?php }?>
	</blockquote>
        <blockquote class="layui-elem-quote news_search">
		<div class="layui-inline">
			<a class="layui-btn layui-btn-normal navAdd_btn">添加导航</a>
		</div>
		
	</blockquote>          
		         <!-- 操作日志 -->
				 
                <div class="layui-form news_list">
                    <table class="layui-table">
					    <colgroup>
						<col width="50">
						<col>
						<col>
						<col>
						<col width="200">
					</colgroup>
					<thead>
						<tr>
						    <th>ID</th>
							<th style="text-align:left;">导航名称</th>
							<th>上级导航</th>
							<th>显示顺序</th>
							<th>链接</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody class="news_content">
					<?php
						$result = mysql_query('select * from aikcms_nav order by id desc');
						while($row = mysql_fetch_array($result)){
						?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_nav_name'];?></td>
							<td><?php echo aik_nav_name($row['aik_nav_papa']);?></td>
							<td><?php echo $row['aik_nav_id'];?></td>
							<td><?php echo $row['aik_nav_url'];?></td>
							<td>
								<a class="layui-btn layui-btn-mini"  href="nav_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini" href="?del=<?php echo $row['id']?>" onclick="return confirm('确认要删除吗？')"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
					</table>
			    </div>
			     <!-- 登录日志 -->
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>