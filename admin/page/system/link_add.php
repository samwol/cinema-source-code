<?php
include('../../../common/basic.php');
include('../admincore/link_add.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>添加友情链接</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body class="childrenBody">
	<form class="layui-form" method="post"  enctype="multipart/form-data">
		<div class="layui-form-item">
			<label class="layui-form-label">友链标题</label>
			<div class="layui-input-block">
				<input class="layui-input newsName" name="aik_link_name" lay-verify="required" placeholder="请输入友链标题" type="text">
			</div>
		</div>
		<div class="layui-form-item">	
				<label class="layui-form-label">LOGO图片</label>
				<div class="layui-inline">
		   <div class="layui-input-inline">
		    	<input  name="aik_link_img" placeholder="这里可以输入网络LOGO图片" class="layui-input search_input"  style="width:102%;" >
		    </div>
         <input type="file" name="aik_link_img"  style="height:38px;"  accept="image/*" />
		</div>
				
			<div class="layui-inline">
				<label class="layui-form-label">站长QQ</label>
				<div class="layui-input-inline">
			<input name="aik_link_qq"  class="layui-input" placeholder="请输入站长QQ" type="text">
				</div>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">链接</label>
			<div class="layui-input-block">
				<input name="aik_link_url" class="layui-input" placeholder="前面一定要带http://" type="text">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn"  name="save">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div></br>
	</form>
<script type="text/javascript" src="../../layui/layui.js"></script>
</body>
</html>