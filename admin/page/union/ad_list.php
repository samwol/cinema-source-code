<?php
include('../../../common/basic.php');
include('../admincore/ad_list.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>广告管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="../../layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="//at.alicdn.com/t/font_tnyc012u2rlwstt9.css" media="all" />
	<link rel="stylesheet" href="../../css/main.css" media="all" />
</head>
<body>
<section class="layui-larry-box">
	<div class="larry-personal">
	    <div class="layui-tab" style="overflow:visible">
            <blockquote class="layui-elem-quote news_search">
      <div class="layui-inline">
			<a class="layui-btn layui-btn-normal adAdd_btn">添加广告</a>
			<a class="layui-btn layui-btn-danger"  href="../module/picture_bed.php"/>免费图床</a>
		</div>
	</blockquote>
            
		         <!-- 操作日志 -->
                <div class="layui-form news_list"><form method="post">
                    <table class="layui-table">
					  <colgroup>
						<col width="50">
						<col>
						<col>
						<col width="200">
						<col width="200">
					
					</colgroup>
					<thead>
						<tr>
							<th style="text-align:left;">ID</th>
							<th>位置</th>
							<th>备注</th>
							<th>调用代码</th>
							<th>操作</th>
						</tr>
					</thead>
		<tbody class="news_content">
					
					<?php

									$sql = 'select * from aikcms_ad order by id desc';
									$pager = page_handle('page',10,mysql_num_rows(mysql_query($sql)));
									$result = mysql_query($sql.' limit '.$pager[0].','.$pager[1].'');
							while($row= mysql_fetch_array($result)){
							?>
						<tr>
							<td><?php echo $row['id'];?></td>
							<td align="left"><?php echo $row['aik_ad_seat'];?></td>
							<td><?php echo $a = $row['aik_ad_remarks']=="" ? "暂无" : $row['aik_ad_remarks'];?></td>
							<td><?php echo "aik_gg(".$row['id'].")";?></td>
							<td>
								<a class="layui-btn layui-btn-mini" href="ad_edit.php?id=<?php echo $row['id']?>"><i class="iconfont icon-edit"></i> 编辑</a>
								<a class="layui-btn layui-btn-danger layui-btn-mini"  onclick="return confirm('确定要进行删除吗')"  href="?del=<?php echo $row['id']?>"><i class="layui-icon"></i> 删除</a>
							</td>
						</tr>
						
						<?php } ?>
					</tbody>	
					</table>
			<div class="layui-form-item">
			<div class="layui-inline">		
				  <div id="page" class="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
						 <?php echo page_show($pager[2],$pager[3],$pager[4],2);?>
						  </div></div>
			</div>
		</div></form>
			     <!-- 登录日志 -->
			    
		    </div>
		</div>
	
</section>
<script type="text/javascript" src="../../layui/layui.js"></script>
<script type="text/javascript" src="../../js/newslist.js"></script>
</body>
</html>