<?php
include('../common/basic.php');
if(isset($_POST['submit'])){
	$aikcms_admin = $_POST['aikcms_admin'];
	$aikcms_password = md5($_POST['aikcms_password']);
	if($_POST['code']!=$_SESSION['authcode']){
		
        alert_href('你的验证码不正确，请重新输入','login.php');    
        exit();
    }
	$sql = "select * from aikcms_basic where aik_admin='$aikcms_admin' and aik_pw='$aikcms_password'";
	$result = mysql_query($sql);
	if(!! $data = mysql_fetch_array($result)){
		setcookie('adminname',$data['aik_admin']);
		header('location:index.php');
		exit();
	}else{
		alert_href('用户名或密码错误','login.php');
		exit();
}}
?>



<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>登录--爱客CMS后台管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="css/login.css" media="all" />
</head>
<body>
	<video class="video-player" preload="auto" autoplay="autoplay" loop="loop" height="100%" width="100%">
	    <source src="page/login/login.mp4" type="video/mp4">
	    <!-- 此视频文件为支付宝所有，在此仅供样式参考，如用到商业用途，请自行更换为其他视频或图片，否则造成的任何问题使用者本人承担，谢谢 -->
	</video>
	<div class="video_mask"></div>
	<div class="login">
	    <h1>爱客CMS-管理登录</h1>
	    <form class="layui-form" method="post">
	    	<div class="layui-form-item">
				<input class="layui-input" name="aikcms_admin" placeholder="用户名" lay-verify="required" type="text" autocomplete="off">
		    </div>
		    <div class="layui-form-item">
				<input class="layui-input" name="aikcms_password" placeholder="密码" lay-verify="required" type="password" autocomplete="off">
		    </div>
		    <div class="layui-form-item form_code">
				<input class="layui-input" name="code" placeholder="验证码" lay-verify="required" type="text" autocomplete="off">
				<div class="code"><img src="page/login/ajax.php" onClick="this.src='page/login/ajax.php?nocache='+Math.random()" style="cursor:hand"  width="116" height="36"></div>
		    </div>
			<button class="layui-btn login_btn" type="submit" name="submit">登录</button>
		</form>
	</div>
	<script type="text/javascript" src="layui/layui.js"></script>
	<script type="text/javascript" src="page/login/login.js"></script>
</body>
</html>