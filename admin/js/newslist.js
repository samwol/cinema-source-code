layui.config({
	base : "js/"
}).use(['form','layer','jquery','laypage'],function(){
	var form = layui.form(),
		layer = parent.layer === undefined ? layui.layer : parent.layer,
		laypage = layui.laypage,
		$ = layui.jquery;

	//加载页面数据
	var newsData = '';
	
	//添加幻灯片
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".newsAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加幻灯片",
				type : 2,
				content : "poster_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回幻灯片列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
    //添加友链
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".linkAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加友链",
				type : 2,
				content : "link_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回友链列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
    //添加导航
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".navAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加导航",
				type : 2,
				content : "nav_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回友链列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加导航
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".navAd3d_btn").click(function(){
			var index = layui.layer.open({
				title : "添加导航2",
				type : 2,
				content : "nav_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回导航列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加用户组
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".usergroupAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加用户组",
				type : 2,
				content : "usergroup_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回用户组列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加用户
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".userAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加用户",
				type : 2,
				content : "user_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回用户列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加用户组升级卡密
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".cardgAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "生成卡密",
				type : 2,
				content : "card_group_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回卡密列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加积分充值卡密
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".cardiAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "生成卡密",
				type : 2,
				content : "card_int_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回卡密列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//发布公告
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".noticeAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "发布公告",
				type : 2,
				content : "notice_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回公告列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加采集规则
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".col_ruleAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加采集规则",
				type : 2,
				content : "col_rule_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回采集列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加采集正则表达式
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".col_ruleurlAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加采集规则",
				type : 2,
				content : "caiji_rule.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回采集规则列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//发布视频
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".videoAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "发布视频",
				type : 2,
				content : "video_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回视频列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加直播源
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".zhiboAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加直播源",
				type : 2,
				content : "zhibo_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回直播列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();
	//添加广告
	//改变窗口大小时，重置弹窗的高度，防止超出可视区域（如F12调出debug的操作）
	$(window).one("resize",function(){
		$(".adAdd_btn").click(function(){
			var index = layui.layer.open({
				title : "添加广告",
				type : 2,
				content : "ad_add.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回广告列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
		})
	}).resize();

	

	//批量删除
	$(".batchDel").click(function(){
		var $checkbox = $('.news_list tbody input[type="checkbox"][name="checked"]');
		var $checked = $('.news_list tbody input[type="checkbox"][name="checked"]:checked');
		if($checkbox.is(":checked")){
			layer.confirm('确定删除选中的信息？',{icon:3, title:'提示信息'},function(index){
				var index = layer.msg('删除中，请稍候',{icon: 16,time:false,shade:0.8});
	            setTimeout(function(){
	            	//删除数据
	            	for(var j=0;j<$checked.length;j++){
	            		for(var i=0;i<newsData.length;i++){
							if(newsData[i].newsId == $checked.eq(j).parents("tr").find(".news_del").attr("data-id")){
								newsData.splice(i,1);
								newsList(newsData);
							}
						}
	            	}
	            	$('.news_list thead input[type="checkbox"]').prop("checked",false);
	            	form.render();
	                layer.close(index);
					layer.msg("删除成功");
	            },2000);
	        })
		}else{
			layer.msg("请选择需要删除的文章");
		}
	})

	//全选
	form.on('checkbox(allChoose)', function(data){
		var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
		child.each(function(index, item){
			item.checked = data.elem.checked;
		});
		form.render('checkbox');
	});

	//通过判断文章是否全部选中来确定全选按钮是否选中
	form.on("checkbox(choose)",function(data){
		var child = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"])');
		var childChecked = $(data.elem).parents('table').find('tbody input[type="checkbox"]:not([name="show"]):checked')
		if(childChecked.length == child.length){
			$(data.elem).parents('table').find('thead input#allChoose').get(0).checked = true;
		}else{
			$(data.elem).parents('table').find('thead input#allChoose').get(0).checked = false;
		}
		form.render('checkbox');
	})

	//是否展示
	form.on('switch(isShow)', function(data){
		var index = layer.msg('修改中，请稍候',{icon: 16,time:false,shade:0.8});
        setTimeout(function(){
            layer.close(index);
			layer.msg("展示状态修改成功！");
        },2000);
	})
 
 
 

	//操作
	$("body").on("click",".news_edit",function(){  //编辑
		var index = layui.layer.open({
				title : "编辑幻灯片",
				type : 2,
				content : "poster_edit.php",
				success : function(layero, index){
					setTimeout(function(){
						layui.layer.tips('点击此处返回幻灯片列表', '.layui-layer-setwin .layui-layer-close', {
							tips: 3
						});
					},500)
				}
			})			
			layui.layer.full(index);
	})
	

})
