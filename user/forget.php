<?php
if (isset($_POST['forget'])) {
	$result = mysql_query('select * from aikcms_user where aik_user_name = "' . $_POST['aik_user_name'] . '"  and  aik_user_qq = "' . $_POST['aik_user_qq'] . '"  ');
	$row=mysql_fetch_array($result);
	if ($row) {
	$sql = 'update aikcms_user  set  aik_user_pw="'.md5($_POST['aik_user_pw']).'" where id = ' . $row['id'] . '';
	if (mysql_query($sql)) {
	alert_href('密码重置成功!','user.php?mode=login');
	} else {
		alert_back('密码重置失败!请重试或者联系管理员！');	
	}
	}else{
	alert_back('用户名或QQ错误，请重试。');	
	}
	
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>注册-<?php echo $aik_name;?></title>
<link rel="stylesheet" href="./user/static/css/normalize.css">
<link rel="stylesheet" href="./user/static/css/login.css">
<link rel="stylesheet" href="./user/static/css/sign-up-login.css">
<link rel="stylesheet" type="text/css" href="http://cdn.bootcss.com/font-awesome/4.6.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./user/static/css/inputEffect.css" />
<link rel="stylesheet" href="./user/static/css/tooltips.css" />
<link rel="stylesheet" href="./user/static/css/spop.min.css" />
<script src="./user/static/js/jquery.min.js"></script>
<script src="./user/static/js/snow.js"></script>
<script src="./user/static/js/jquery.pure.tooltips.js"></script>
<script src="./user/static/js/spop.min.js"></script>
<script>	
	$(function() {	
		$('#login #login-password').focus(function() {
			$('.login-owl').addClass('password');
		}).blur(function() {
			$('.login-owl').removeClass('password');
		});
		$('#login #find-password').focus(function() {
			$('.find-owl').addClass('password');
		}).blur(function() {
			$('.find-owl').removeClass('password');
		});
		$('#login #find-repassword').focus(function() {
			$('.find-owl').addClass('password');
		}).blur(function() {
			$('.find-owl').removeClass('password');
		});
		$('#login #forget-password').focus(function() {
			$('.forget-owl').addClass('password');
		}).blur(function() {
			$('.forget-owl').removeClass('password');
		});
	});	
</script>
<script type="text/javascript">
function find_form(){
    var username = document.getElementById("forget-username");
		if(username.value == ""){
			alert("用户名不能为空！");
			return false;
		}
	var password = document.getElementById("forget-password");
		if(password.value == ""){
			alert("密码不能为空！");
			return false;
		}
	var userqq = document.getElementById("forget-userqq");
		if(userqq.value == ""){
			alert("请务必填入QQ，找回密码要用的！");
			return false;
		}
	var re = /^[0-9]+.?[0-9]*$/;
        if(!re.test(userqq.value)){ 
        alert("QQ格式错误！");
        return false;
        }
		//用户名只能是15位以下的字母或数字
	var regExp = new RegExp("^[a-zA-Z0-9_]{1,15}$");
		if(!regExp.test(username.value)){
			alert("用户名必须为15位以下的字母或数字！");
			return false;
		}
}
	
	

</script>
<style type="text/css">
html{width: 100%; height: 100%;}

body{

	background-repeat: no-repeat;

	background-position: center center #2D0F0F;

	background-color: #00BDDC;

	background-image: url(./user/static/images/snow.jpg);

	background-size: 100% 100%;

}

.snow-container { position: fixed; top: 0; left: 0; width: 100%; height: 100%; pointer-events: none; z-index: 100001; }

</style>
</head>
<body>
	<!-- 雪花背景 -->
	<div class="snow-container"></div>
	<!-- 登录控件 -->
	<div id="login">
		<div class="wrapper">
				<form method="post" class="container offset1 loginform" onsubmit="return find_form();">
					<!-- 猫头鹰控件 -->
					<div id="owl-login" class="forget-owl">
						<div class="hand"></div>
						<div class="hand hand-r"></div>
						<div class="arms">
							<div class="arm"></div>
							<div class="arm arm-r"></div>
						</div>
					</div>
					<div class="pad input-container">
						<section class="content">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="text" id="forget-username" name="aik_user_name" placeholder="请输入用户名"/>
								<label class="input__label input__label--hideo" for="forget-username">
									<i class="fa fa-fw fa-user icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="text" id="forget-userqq"  name="aik_user_qq" placeholder="请输入你的QQ"/>
								<label class="input__label input__label--hideo" for="forget-code">
									<i class="fa fa-fw fa-wifi icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="password" id="forget-password" name="aik_user_pw" placeholder="请重置密码" />
								<label class="input__label input__label--hideo" for="forget-password">
									<i class="fa fa-fw fa-lock icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
						</section>
					</div>
					<div class="form-actions">
						<a class="btn pull-left btn-link text-muted" href="user.php?mode=login">返回登录</a>
						<button  class="btn btn-primary "  name="forget" style="color:white;">重置密码</button>
					</div>
				</form>
		</div>
	</div>
</body>
</html>