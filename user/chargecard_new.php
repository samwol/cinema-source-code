<?php include './user/data/config.php';?> 
 <!DOCTYPE html> 
 <html> 
 <head> 
 <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0"> 
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
 <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
 <meta name="renderer" content="webkit">
<title>我要充值-<?php echo $aik_name;?>-控制面板</title>
<style type="text/css">
.box {
  position: relative;
  line-height: 30px;
  width: 20%;
  float:left;
}

input[type="radio"] {
  width: 20px;
  height: 20px;
  opacity: 0;
}

label {
  position: absolute;
  left: 5px;
  top: 8px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  border: 1px solid #999;
}

/*设置选中的input的样式*/
/* + 是兄弟选择器,获取选中后的label元素*/
input:checked+label {
  background-color: #006eb2;
  border: 1px solid #006eb2;
}

input:checked+label::after {
  position: absolute;
  content: "";
  width: 5px;
  height: 10px;
  top: 3px;
  left: 6px;
  border: 2px solid #fff;
  border-top: none;
  border-left: none;
  transform: rotate(45deg)
}

        </style>		
<?php include 'header.php'?>    
</head>
<body>
     <?php include 'head.php'?> 
<div class="pd20-1 bgh-1">
<div class="user-mian container">
    <div class="row">
   <?php include 'leftlist.php'?>
<div class="col-xs-12 col-sm-10">
<div class="bgb border mb20 clearfix">
<ul class="user-tab f-18 clearfix">
    <li><a href="user.php?mode=account">财务信息</a></li>
    <li><a href="user.php?mode=charge">充值记录</a></li>
    <li class="active"><a href="user.php?mode=chargecard_new">我要充值</a></li>
</ul>
<div class="pd20">
<div class="mb20 border">
    <h2 class="bc-hui lh-38 f-18 border-b title-i"><i class="fa fa-yen (alias)"></i> 在线充值 <span class="f-12">(支持支付宝和微信支付)</span></h2>
<dl class="pd20">
<form role="form"  action="user/pay/Send.php" method="post" id="charge-form">							
<input type="hidden"  name="orderno"  value='<?php echo date("YmdHis",time()).str_rand();?>'>
<div class="box">
        <input id="item1" type="radio" name="bacode" value="ALIPAY" checked>
        <label for="item1"></label>
        <span style="margin-left: 10px">支付宝</span>
      </div>
      <div  class="box">
        <input id="item2" type="radio" name="bacode" value="WEIXINPAY">
        <label for="item2"></label>
        <span style="margin-left: 10px">微信</span>
      </div><div  class="box">
        <input id="item3" type="radio" name="bacode" value="QQPAY">
        <label for="item3"></label>
        <span style="margin-left: 10px">QQ钱包</span>
      </div>

<br><br><br>

<div class="tx-form2">
<input class="tx-input2"  id="charge_money"  name="pamount"  placeholder="请输入一个整数后点击按钮即可进行充值">
<input id="Bin" type="submit" value="提 交" class="tx-btn2" /></div>
</form>
</dl>
</div>     


<div class="mb20 border">
    <h2 class="bc-hui lh-38 f-18 border-b title-i"><i class="fa fa-yen (alias)"></i> 充值卡充值 <span class="f-12">(可参见活动获得充值卡)</span></h2>
    <dl class="pd20">
 <form role="form"  action="#" method="post" id="charge-form" class="tx-form2">
<input class="tx-input2"  id="charge_code" name="charge_code"  placeholder="请输入充值卡卡号点击充值按钮即可充值">
<button type="button" onclick="zcenter_checkchargecard()" class="tx-btn2">充值</button>
</form>
</dl>
</div>   

 
 
</div>  
 </div>
     <p class="img-d"><a href="https://www.txcstx.cn/user/charge.php"><img src="https://www.txcstx.cn/user/img/vip.png"></a></p>  
</div> 
</div></div></div>
 <?php include 'foot.php'?>

</body>
</html><!--38.52 ms , 7 query , 3957kb memory , 0 error-->