<?php
$affid=$_COOKIE["affid"]; 
if (isset($_POST['register'])) {
	$result = mysql_query('select * from aikcms_user where aik_user_name = "' . $_POST['aik_user_name'] . '"');
	if (mysql_fetch_array($result)) {
		alert_back('帐号重复，请输入新的帐号。');
	}
	$_data['aik_user_name'] = $_POST['aik_user_name'];
	$_data['aik_user_pw'] = md5($_POST['aik_user_pw']);
	$_data['aik_user_qq'] = $_POST['aik_user_qq'];
	$_data['aik_user_int'] = $aik_user_initint;
	$_data['aik_user_ip'] = getIP();
	$_data['aik_user_time'] = time();

	$sqlip="select * from aikcms_user where aik_user_ip='".getIP()."' limit 1";
	$rowip=mysql_fetch_array(mysql_query($sqlip));
	if((!$rowip)&&$affid){
	$newint=get_user_int($affid)+$aik_user_affint;
	mysql_query('UPDATE `aikcms_user` SET `aik_user_int`='.$newint.' WHERE id='.$affid.'');
	}
	$str = arrtoinsert($_data);
	$sql = 'insert into aikcms_user (' . $str[0] . ') values (' . $str[1] . ')';
	if (mysql_query($sql)) {
    alert_href('恭喜注册成功!', 'user.php?mode=login');
	} else {
	    alert_back('注册失败!');
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>注册-<?php echo $aik_name;?></title>
<link rel="stylesheet" href="./user/static/css/normalize.css">
<link rel="stylesheet" href="./user/static/css/login.css">
<link rel="stylesheet" href="./user/static/css/sign-up-login.css">
<link rel="stylesheet" type="text/css" href="http://cdn.bootcss.com/font-awesome/4.6.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./user/static/css/inputEffect.css" />
<link rel="stylesheet" href="./user/static/css/tooltips.css" />
<link rel="stylesheet" href="./user/static/css/spop.min.css" />
<script src="./user/static/js/jquery.min.js"></script>
<script src="./user/static/js/snow.js"></script>
<script src="./user/static/js/jquery.pure.tooltips.js"></script>
<script src="./user/static/js/spop.min.js"></script>
<script>	
	$(function() {	
		$('#login #login-password').focus(function() {
			$('.login-owl').addClass('password');
		}).blur(function() {
			$('.login-owl').removeClass('password');
		});
		$('#login #register-password').focus(function() {
			$('.register-owl').addClass('password');
		}).blur(function() {
			$('.register-owl').removeClass('password');
		});
		$('#login #register-repassword').focus(function() {
			$('.register-owl').addClass('password');
		}).blur(function() {
			$('.register-owl').removeClass('password');
		});
		$('#login #forget-password').focus(function() {
			$('.forget-owl').addClass('password');
		}).blur(function() {
			$('.forget-owl').removeClass('password');
		});
	});	
</script>
<script type="text/javascript">
function reg_form(){
    var username = document.getElementById("register-username");
		if(username.value == ""){
			alert("用户名不能为空！");
			return false;
		}
    var password = document.getElementById("register-password");
		repassword = document.getElementById("register-repassword");
		//判断用户名密码是否为空
		if(password.value == ""){
			alert("密码不能为空！");
			return false;
		}else{
			if(password.value != repassword.value){
				alert("两次输入的密码不一致！");
				return false;
			}
		}
	var userqq = document.getElementById("register-userqq");
		if(userqq.value == ""){
			alert("请务必填入QQ，找回密码要用的！");
			return false;
		}
	var re = /^[0-9]+.?[0-9]*$/;
        if(!re.test(userqq.value)){ 
        alert("QQ格式错误！");
        return false;
        }
		//用户名只能是15位以下的字母或数字
	var regExp = new RegExp("^[a-zA-Z0-9_]{1,15}$");
		if(!regExp.test(username.value)){
			alert("用户名必须为15位以下的字母或数字！");
			return false;
		}
}
	
	

</script>
<style type="text/css">
html{width: 100%; height: 100%;}

body{

	background-repeat: no-repeat;

	background-position: center center #2D0F0F;

	background-color: #00BDDC;

	background-image: url(./user/static/images/snow.jpg);

	background-size: 100% 100%;

}

.snow-container { position: fixed; top: 0; left: 0; width: 100%; height: 100%; pointer-events: none; z-index: 100001; }

</style>
</head>
<body>
	<!-- 雪花背景 -->
	<div class="snow-container"></div>
	<!-- 登录控件 -->
	<div id="login">
		<div class="wrapper">
			<!-- 注册页面 -->
				<form method="post" class="container offset1 loginform" onsubmit="return reg_form();">
					<!-- 猫头鹰控件 -->
					<div id="owl-login" class="register-owl">
						<div class="hand"></div>
						<div class="hand hand-r"></div>
						<div class="arms">
							<div class="arm"></div>
							<div class="arm arm-r"></div>
						</div>
					</div>
					<div class="pad input-container">
						<section class="content">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="text" name="aik_user_name" id="register-username" 
									autocomplete="off" placeholder="请输入用户名" maxlength="15"/>
								<label class="input__label input__label--hideo" for="register-username">
									<i class="fa fa-fw fa-user icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="password" name="aik_user_pw"  id="register-password" placeholder="请输入密码" maxlength="15"/>
								<label class="input__label input__label--hideo" for="register-password">
									<i class="fa fa-fw fa-lock icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="password" name="repassword" id="register-repassword" placeholder="请确认密码" maxlength="15"/>
								<label class="input__label input__label--hideo" for="register-repassword">
									<i class="fa fa-fw fa-lock icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="text" name="aik_user_qq" id="register-userqq" autocomplete="off" placeholder="请输入你的QQ"/>
								<label class="input__label input__label--hideo" for="register-code">
									<i class="fa fa-fw fa-wifi icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
						</section>
					</div>
					<div class="form-actions">
						<a class="btn pull-left btn-link text-muted" href="user.php?mode=login">返回登录</a>
						<button  class="btn btn-primary"  name="register" >注册</button>
					</div>
				</form>
		</div>
	</div>
</body>
</html>