<?php
/**  
 * DEMO 2.0 - 蓝讯数卡商户合作开发技术文档规范 
 *
 * Website - www.lcardy.com
 * Company - 海口环讯信息科技有限公司
 *文件列表说明。
 *|----SendTest.html(提供给商户测试用的首页)
 *|----Send.php(支付请求文件，通过此文件发起寄售请求，商家可以在此文件中写入自己的订单信息等，然后把请求提交给蓝讯数卡API接口)
 *|----merchantAttribute.php(商家属性文件，商家可以在此文件中修改商户编号和密钥信息)
 *|----callBack.php(异步通知结果回文件，通过此文件商家判断对应订单的支付状态，并且根据结果修改自己数据库中的订单状态)
 *|----callBackPage.php(同步通知结果返回文件，通过此文件商家判断对应订单的支付状态，并且根据结果修改自己数据库中的订单状态)
 */
  	  include 'Common.php';
	  include 'merchantAttribute.php';
 	  
	  #$mercid             =$merc_id;                                   
	  $mercid             =$_REQUEST["mercid"];
 	  $merckey            =$merc_key; 
	  $version            =$_REQUEST["version"]; 
	  $signtype           =$_REQUEST["signtype"]; 
	  $code           	  =$_REQUEST["code"]; 
	  $myorderno          =$_REQUEST["myorderno"]; 
	  $orderamount        =$_REQUEST["orderamount"]; 
	  $orderno            =$_REQUEST["orderno"]; 
	  $ordertime          =$_REQUEST["ordertime"]; 
	  $exinf              =$_REQUEST["exinf"]; 
	  $notifytype         =$_REQUEST["notifytype"]; 
	  $bankseqno          =$_REQUEST["bankseqno"]; 
	  $sign           	  =$_REQUEST["sign"]; 
	  
	  
	  $signStr	          ='mercid='.$mercid;
	  $signStr	          =$signStr.'&code='.$code;
	  $signStr	          =$signStr.'&myorderno='.$myorderno;
	  $signStr	          =$signStr.'&orderamount='.$orderamount;
	  $signStr	          =$signStr.'&orderno='.$orderno;
	  $signStr	          =$signStr.'&ordertime='.$ordertime;
	  $signStr	          =$signStr.'&signtype='.$signtype;
	  $signStr	          =$signStr.'&version='.$version;
	  $signStr	          =$signStr.'&notifytype='.$notifytype;
	  $signStr	          =$signStr.'&bankseqno='.$bankseqno;
	  $signStr	          =$signStr.'&merckey='.$merckey;
	  
	  $signValue          =md5sign($signStr); 
	  
	  if ( $signValue==$sign)
        {
            //商户接收到通知结果后，首先通过sign验证各个参数正确性，
            //然后通过code来处理本地单据：当code=1时按照成功支付金额orderamount来处理本地资金。
            //其它情况都为失败。用户处理完成后请向本地接收页面写入"success"
	    	echo 'success';
            if ($code == "1")
            {
            	//在这里添加或处理你的业务逻辑
                //在接收到结果通知后，判断是否进行过业务逻辑处理，不要重复进行业务逻辑处理
            	echo "<br>支付成功";
            	echo "<br>订单号：".$orderno;
                echo "<br>成功金额：".$orderamount;
            }
            else
            {
            	echo "<br>支付失败";
            	echo "<br>订单号：".$orderno;
            }
        }
        else
        {
            //MD5数据验证不正确
        	echo 'Error Sign';
        }
  ?>