$(function(){
    var winr=$(window); 
    if(winr.width()>=800){
        $(".nav>li").hover(function() {
            if($(this).find("li").length > 0){
                $(this).children("ul").stop(true, true).slideDown();
                $(this).addClass("hover");
            }
        },function() {
            $(this).children("ul").stop(true, true).slideUp();
            $(this).removeClass("hover");
        });
    }else{
        $(".nav>li").each(function(){ 
            if($(this).find("li").length > 0){
                $(this).append("<i class='fa fa-angle-down fa-angle-up'></i>");
            }
            var _this = this
            $(this).find("i").click(function() {
                 $(_this).find("i.fa-angle-up").toggleClass("fa-angle-down");
                 $(_this).find("ul").slideToggle();
            });
        });

    }
});

$(function(){
    var navr=$(".header"); 
    var winr=$(window); 
    var scr=$(document);
    winr.scroll(function(){
        if(scr.scrollTop()>=40){
            navr.addClass("fixednavr"); 
            $(".index-1").css({"padding-top":"90px"});
            $(".xf-bg-wrap").css({"padding-top":"90px"});
        }else{
            navr.removeClass("fixednavr");
            $(".index-1").css({"padding-top":"20px"});
            $(".xf-bg-wrap").css({"padding-top":"0"});
        }
    })  
})



$(".menu-search").on("click", function(e){
    if($(".search-main").is(":hidden")){
        $(".search-main").slideDown();
        $(".menu-search i").addClass("fa-close (alias)");
    }else{
        $(".search-main").slideUp();
        $(".menu-search i").removeClass("fa-close (alias)");
    }

    $(document).one("click", function(){
        $(".search-main").slideUp();
        $(".menu-search i").removeClass("fa-close (alias)");
    });

    e.stopPropagation();
});
$(".search-main").on("click", function(e){
    e.stopPropagation();
});


$(".foot-weixin").hover(function(){
    $(".foot-weixin>div").toggle();
});
$(".foot-qqun").hover(function(){
    $(".foot-qqun>div").toggle();
});

$(function(){
	var fixedh = $(document);
	$(window).scroll(function(){
	    if(fixedh.scrollTop()>=900){
		    $('#gotop').fadeIn();
	    }else{
		    $('#gotop').fadeOut();
	    }
	}) 
	$("#gotop").click(function(){
        $('body,html').animate({scrollTop:0},300);
    })
});


$(function(){
    var surl = location.href;
    var surl2 = $(".place a:eq(1)").attr("href");
    $("#example-navbar-collapse ul li a").each(function() {
        if ($(this).attr("href")==surl || $(this).attr("href")==surl2) $(this).parent().addClass("on")
    });
});

$(function(){
    var surrl = location.href;
    var surrl2 = $(".place a:eq(2)").attr("href");
    $(".column dd ul li a").each(function() {
        if ($(this).attr("href")==surrl || $(this).attr("href")==surrl2) $(this).parent().addClass("on");
    });
    $(".mbdh ul li a").each(function() {
        if ($(this).attr("href")==surrl || $(this).attr("href")==surrl2) $(this).parent().addClass("on");
    });
    $(".page-nav ul li a").each(function() {
        if ($(this).attr("href")==surrl || $(this).attr("href")==surrl2) $(this).parent().addClass("on");
    });
});

$(function(){
    var float=$(".rightggbox"); 
    var box=$(".sidebar"); 
    var boxw=$(".sidebar").width();
    var winr=$(window); 
    var scr=$(document);
    if(winr.width()>=1210){
        winr.scroll(function(){
        if(scr.scrollTop() >= (box.height() + float.height() -150 ) ){
            float.addClass("side-fixed"); 
            float.css({ "width": boxw}); 
        }else{
            float.removeClass("side-fixed");
        }
    }) ;
    }
});


zbp.plugin.unbind("comment.reply", "system");
zbp.plugin.on("comment.reply", "txcstx", function(id) {
	var i = id;
	$("#inpRevID").val(i);
	var frm = $('#comment'),
		cancel = $("#cancel-reply");

	frm.before($("<div id='temp-frm' style='display:none'>")).addClass("reply-frm");
	$('#AjaxComment' + i).before(frm);

	cancel.show().click(function() {
		var temp = $('#temp-frm');
		$("#inpRevID").val(0);
		if (!temp.length || !frm.length) return;
		temp.before(frm);
		temp.remove();
		$(this).hide();
		frm.removeClass("reply-frm");
		return false;
	});
	try {
		$('#txaArticle').focus();
	} catch (e) {}
	return false;
});

