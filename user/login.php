<?php
if(isset($_POST['login'])){
	$aik_user_name = $_POST['aik_user_name'];
	$aik_user_pw = md5($_POST['aik_user_pw']);
	$sql = "select * from aikcms_user where aik_user_name='$aik_user_name' and aik_user_pw='$aik_user_pw'";
	$result = mysql_query($sql);
	if(!! $data = mysql_fetch_array($result)){
		if($data['aik_user_on']=="1"){
		setcookie('username',$data['aik_user_name']);
		header('location:user.php?mode=index');
		exit();
		}else{
			alert_href('该账号已被锁定，请联系管理员！','user.php?mode=login');
		    exit();
		}
	}else{
		alert_href('用户名或密码错误','user.php?mode=login');
		exit();
}}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>登录-<?php echo $aik_name;?></title>
<link rel="stylesheet" href="./user/static/css/normalize.css">
<link rel="stylesheet" href="./user/static/css/login.css">
<link rel="stylesheet" href="./user/static/css/sign-up-login.css">
<link rel="stylesheet" type="text/css" href="http://cdn.bootcss.com/font-awesome/4.6.0/css/font-awesome.min.css">
<link rel="stylesheet" href="./user/static/css/inputEffect.css" />
<link rel="stylesheet" href="./user/static/css/tooltips.css" />
<link rel="stylesheet" href="./user/static/css/spop.min.css" />
<script src="./user/static/js/jquery.min.js"></script>
<script src="./user/static/js/snow.js"></script>
<script src="./user/static/js/jquery.pure.tooltips.js"></script>
<script src="./user/static/js/spop.min.js"></script>
<script>	
	$(function() {	
		$('#login #login-password').focus(function() {
			$('.login-owl').addClass('password');
		}).blur(function() {
			$('.login-owl').removeClass('password');
		});
		$('#login #register-password').focus(function() {
			$('.register-owl').addClass('password');
		}).blur(function() {
			$('.register-owl').removeClass('password');
		});
		$('#login #register-repassword').focus(function() {
			$('.register-owl').addClass('password');
		}).blur(function() {
			$('.register-owl').removeClass('password');
		});
		$('#login #forget-password').focus(function() {
			$('.forget-owl').addClass('password');
		}).blur(function() {
			$('.forget-owl').removeClass('password');
		});
	});	
</script>
<script type="text/javascript">
function login_form(){
    var username = document.getElementById("login-username");
		if(username.value == ""){
			alert("用户名不能为空！");
			return false;
		}
    var password = document.getElementById("login-password");
		//判断用户名密码是否为空
		if(password.value == ""){
			alert("密码不能为空！");
			return false;
		}
}
	
	

</script>
<style type="text/css">
html{width: 100%; height: 100%;}

body{

	background-repeat: no-repeat;

	background-position: center center #2D0F0F;

	background-color: #00BDDC;

	background-image: url(./user/static/images/snow.jpg);

	background-size: 100% 100%;

}

.snow-container { position: fixed; top: 0; left: 0; width: 100%; height: 100%; pointer-events: none; z-index: 100001; }

</style>
</head>
<body>
	<!-- 雪花背景 -->
	<div class="snow-container"></div>
	<!-- 登录控件 -->
	<div id="login">
		<div class="wrapper">
			<!-- 登录页面 -->
				<form class="container offset1 loginform" method="post"  onsubmit="return login_form();">
					<!-- 猫头鹰控件 -->
					<div id="owl-login" class="login-owl">
						<div class="hand"></div>
						<div class="hand hand-r"></div>
						<div class="arms">
							<div class="arm"></div>
							<div class="arm arm-r"></div>
						</div>
					</div>
					<div class="pad input-container">
						<section class="content">
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="text" name="aik_user_name" id="login-username" 
									autocomplete="off" placeholder="请输入用户名" tabindex="1" maxlength="15" />
								<label class="input__label input__label--hideo" for="login-username">
									<i class="fa fa-fw fa-user icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
							<span class="input input--hideo">
								<input class="input__field input__field--hideo" type="password" id="login-password" name="aik_user_pw" placeholder="请输入密码" tabindex="2" maxlength="15"/>
								<label class="input__label input__label--hideo" for="login-password">
									<i class="fa fa-fw fa-lock icon icon--hideo"></i>
									<span class="input__label-content input__label-content--hideo"></span>
								</label>
							</span>
						</section>
					</div>
				<div class="form-actions" style="text-align: left;">
				<?php if($aik_qqlogin=="1"){?>
					<a id="login_QQ" class="btn btn-info" href="user.php?mode=qqlogin">
						<img src="./user/static/images/qq32.png" style="width:20px;margin-top:-4px;" />&emsp;QQ登录
				</a><?php }?>
					<button  class="btn btn-danger pull-right "  name="login" style="color:white;">登录</button><br>
					</div>
					<div class="form-actions" style="padding: 0 30px;">    

						<a tabindex="4" class="btn pull-left btn-link text-muted" href="user.php?mode=forget">忘记密码?</a>
						<a tabindex="5" class="btn btn-link text-muted" href="user.php?mode=reg">注册</a>
						
					</div>
					</form>
		</div>
		
	</div>
</body>
</html>