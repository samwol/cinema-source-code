﻿<?php
include('common/basic.php');
define('TOKEN', $aik_wx_token); //定义常量
define('DOMAIN', $aik_domain); //定义常量
define('REPLY', $aik_wx_reply); //定义常量
define('IMG', $aik_wx_img); //定义常量
define('NAME', $aik_name); //定义常量
$wechatObj = new wechatCallbackapiTest();//实例化类                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
if (isset($_GET['echostr'])) { //如果随机字符串存在
    $wechatObj->valid(); //执行wechatObj类下的valid函数
}else{
    $wechatObj->responseMsg(); //如果未得到随机字符串，执行wechatObj类下的responseMsg函数
}

class wechatCallbackapiTest  //定义类
{
    public function valid()  //定义valid函数
    {
        $echoStr = $_GET["echostr"];  //定义变量$echoStr 为获得的字符串
        if($this->checkSignature()){  //调用当前类里的chekSignature函数
            echo $echoStr;  //输出echoStr的值
            exit;
        }
    }

    private function checkSignature()  //定义checkSignature函数
    {
        $signature = $_GET["signature"];  //定义变量signature为获得的signature
        $timestamp = $_GET["timestamp"];  //获取时间戳
        $nonce = $_GET["nonce"];          //获取随机数

        $token = TOKEN; //获得token
        $tmpArr = array($token, $timestamp, $nonce);//定义数组tmpArr包含token值、时间戳、随机数
        sort($tmpArr, SORT_STRING);  //对数组进行升序排列
        $tmpStr = implode( $tmpArr );  //将数组组合为字符串
        $tmpStr = sha1( $tmpStr );  //计算字符串的sha1散列

        if( $tmpStr == $signature ){ //如果计算的散列与获取的加密签名一致
            return true;  //返回真
        }else{
            return false;  //不一致，返回假
        }
    }

  public function responseMsg()
    {

		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];


		if (!empty($postStr)){

                libxml_disable_entity_loader(true);
              	$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
                $fromUsername = $postObj->FromUserName;
                $toUsername = $postObj->ToUserName;
                $keyword = trim($postObj->Content);

				$event = $postObj->Event;			
                $time = time();
                $textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";    
				


				switch($postObj->MsgType)
				{
					case 'event':

						if($event == 'subscribe')
						{
						//关注后的回复
												$contentStr = REPLY;


							$msgType = 'text';
							$textTpl = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
							echo $textTpl;

						}
						break;
					case 'text':
						{
							$newsTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[news]]></MsgType>
							<ArticleCount>1</ArticleCount>
							<Articles>
							<item>
							<Title><![CDATA[%s]]></Title> 
							<Description><![CDATA[%s]]></Description>
							<PicUrl><![CDATA[%s]]></PicUrl>
							<Url><![CDATA[%s]]></Url>
							</item>							
							</Articles>
							</xml>";	
 						if($keyword<>"")
						{
										$title = '您要看的《'.$keyword.'》,给您找到以下结果,点击查看！';
										
										$des1 ="";
										//图片地址
										$picUrl1 = ''.IMG.'';
										//跳转链接
										$url = ''.DOMAIN.'index.php?mode=search&wd='.$keyword.'';

										$resultStr= sprintf($newsTpl, $fromUsername, $toUsername, $time, $title, $des1, $picUrl1, $url) ;
									
										echo $resultStr; 	
						}
												$contentStr = " \r\n 输入电影名如：画江湖之不良人 即可在线观看！\r\n ";


							$msgType = 'text';
							$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
							echo $resultStr;
						}					
						break;
					default:
						break;
				}						

        }else {
        	echo "你好！欢迎进入【".NAME."】微信公众号";
        	exit;
        }
    }
}
?>