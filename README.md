# 影院源码



会员尝鲜中心,自动尝鲜、手动尝鲜、一键资源网采集,支持淘宝客和京东客设置,对接公众号，支付管理,
模块管理，广告联盟设置,留言管理,公告设置,缓存管理,伪静态等

#全站修复360影视采集规则

#更新资源网采集展示

#修复影片列表类型及上下页问题

#更新修复视频解析规则

#更新修复YY娱乐视频解析

#其他代码缓存生成优化源码可支持上传二级目录访问，

更新内容：

①：后台采用layuicms框架
②：增加QQ登录模块
③：增加全站缓存和伪静态功能
④：增加打赏功能模块
⑤：增加支付模块
⑥：增加淘宝客和京东客联盟
⑦：修复播放页弹窗问题
⑧：修复尝鲜列表和自动尝鲜分页问题



#### 软件架构

安装路径 访问域名/common/install/安装

爱客影视CMS管理系统米酷优化版

源码简介：

爱客影视CMS管理系统米酷优化版,修复了采集功能、尝鲜可自动采集尝鲜、手动尝鲜采集、
也可单独采集和上传。

推荐搭建环境：  PHP 5.6

Apache环境 伪静态规则
<IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteRule ^vod/(.*).html$ index.php?mode=play&vod=$1
        RewriteRule ^vid/(.*).html$ index.php?mode=play&vid=$1
        RewriteRule ^video(.*)$ index.php?mode=video&net=$1
</IfModule>



