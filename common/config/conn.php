<?php
return array( 
    //数据库配置
    'db_type' => 'pdo',     	// 数据库链接扩展 , 支持 pdo | mysqli | mysql
    'db_host' => 'localhost',  // 服务器地址
    'db_name' => 'ak',// 数据库名
    'db_user' => 'ak',       // 用户名
    'db_pwd' => '123456',       		// 密码
    'db_port' => 3306,         // 端口
    'db_prefix' => 'aikcms_',        // 数据库表前缀
	'db_charset' => 'UTF8',        // 数据库编码
);
?>