﻿-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019 年 03 月 19 日 07:16
-- 服务器版本: 5.5.53
-- PHP 版本: 5.4.45

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `aikcms`
--

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_ad`
--

CREATE TABLE IF NOT EXISTS `aikcms_ad` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aik_ad_remarks` varchar(255) NOT NULL,
  `aik_ad_seat` varchar(255) NOT NULL,
  `aik_ad_code` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `aikcms_ad`
--

INSERT INTO `aikcms_ad` (`id`, `aik_ad_remarks`, `aik_ad_seat`, `aik_ad_code`) VALUES
(1, '大淘客', '播放器上', '&lt;a href=&quot;http://www.domain.com&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;/upload/krHHk6.png&quot; style=&quot;width:100%;&quot;&gt;&lt;/a&gt;'),
(2, '点击绿条', '播放器下', '&lt;style&gt; \r\n .black_overlay{  display: none;  position: absolute;  top: 0%;  left: 0%;  width: 100%;  height: 100%;  background-color: black;  z-index:1001;  -moz-opacity: 0.8;  opacity:.80;  filter: alpha(opacity=80);  }  .white_content {  display: none;  position: absolute;  top: 25%;  left: 25%;  width: 50%;   height: auto; padding: 16px;  border: 16px solid orange;  background-color: white;  z-index:1002;  overflow: auto;  } .rewards-popover-close{position:absolute;top:10px;right:10px;cursor:pointer; width:20px; height:20px;}.rewards-popover-close:hover{color:#666} &lt;/style&gt; \r\n&lt;a href=&quot;javascript:void(0)&quot; onclick=&quot;document.getElementById(''light'').style.display=''block'';document.getElementById(''fade'').style.display=''block''&quot;&gt;&lt;p style=&quot;text-align:center;color: #fff;font-size: 10px;background: #6ED56C;padding:11px 8px;border-radius: 2px;&quot;&gt;点击关注“爱客影院”官方微信，看电影更方便!&lt;/p&gt;&lt;/a&gt;\r\n&lt;div id=&quot;light&quot; class=&quot;white_content&quot;&gt; \r\n  &lt;img src=&quot;http://v.xchwl.cn/2.png&quot; width=&quot;100%&quot; height=&quot;100%&quot;&gt;\r\n    &lt;a href=&quot;javascript:void(0)&quot; onclick=&quot;document.getElementById(''light'').style.display=''none'';document.getElementById(''fade'').style.display=''none''&quot;&gt; \r\n    &lt;span class=&quot;rewards-popover-close&quot; etap=&quot;rewards-close&quot;&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt; \r\n&lt;div id=&quot;fade&quot; class=&quot;black_overlay&quot;&gt; \r\n&lt;/div&gt;'),
(3, '大淘客', '分类列表下', '&lt;a href=&quot;https://www.domain.com&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;/upload/krHHk6.png&quot; style=&quot;width:100%;&quot;&gt;&lt;/a&gt;'),
(4, '', '播放页侧边', '&lt;div class=&quot;widget widget-textasst&quot;&gt;&lt;a class=&quot;style02&quot; href=&quot;http://qqds.xchwl.cn&quot; target=&quot;_blank&quot;&gt;&lt;strong&gt;源码出售&lt;/strong&gt;&lt;h2&gt;更好的视频主题&lt;/h2&gt;&lt;p&gt;1.扁平化、简洁风、多功能配置，优秀的电脑、平板、手机支持，响应式布局，不同设备不同展示效果...2.视频全自动采集，不需人工干预，懒人必备！&lt;/p&gt;&lt;/a&gt;&lt;/div&gt;\r\n&lt;div class=&quot;widget widget-textasst&quot;&gt;&lt;a class=&quot;style03&quot;  href=&quot;http://www.domain.com&quot; target=&quot;_blank&quot;&gt;&lt;strong&gt;建站服务&lt;/strong&gt;&lt;/br&gt;&lt;/br&gt;&lt;img src=&quot;upload/aly.gif&quot;&gt;&lt;/a&gt;&lt;/div&gt;'),
(5, '', '用户中心', '&lt;a href=&quot;http://www.domain.com; target=&quot;_blank&quot;&gt;&lt;img src=&quot;/upload/006Fzy5igy1g0xxfcgge9j30k802i3zi.jpg&quot; style=&quot;width:100%;&quot;&gt;&lt;/a&gt;\r\n');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_basic`
--

CREATE TABLE IF NOT EXISTS `aikcms_basic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_name` varchar(255) DEFAULT NULL,
  `aik_domain` varchar(255) DEFAULT NULL,
  `aik_title` varchar(255) DEFAULT NULL,
  `aik_keywords` varchar(255) DEFAULT NULL,
  `aik_desc` varchar(255) DEFAULT NULL,
  `aik_shouquan` varchar(255) DEFAULT NULL,
  `aik_template` varchar(255) DEFAULT NULL,
  `aik_mtemplate` varchar(255) NOT NULL,
  `aik_icp` varchar(255) DEFAULT NULL,
  `aik_tongji` text,
  `aik_changyan` text,
  `aik_jiexi1` varchar(255) DEFAULT NULL,
  `aik_jiexi2` varchar(255) DEFAULT NULL,
  `aik_jiexi3` varchar(255) DEFAULT NULL,
  `aik_jiexi4` varchar(255) DEFAULT NULL,
  `aik_jiexi5` varchar(255) DEFAULT NULL,
  `aik_cache` varchar(255) DEFAULT NULL,
  `aik_weijing` varchar(255) DEFAULT NULL,
  `aik_onlym` varchar(255) DEFAULT NULL,
  `aik_postercome` varchar(255) DEFAULT NULL,
  `aik_wxsk` varchar(255) DEFAULT NULL,
  `aik_zfbsk` varchar(255) DEFAULT NULL,
  `aik_wxgzh` varchar(255) DEFAULT NULL,
  `aik_qqjq` varchar(255) DEFAULT NULL,
  `aik_appxz` varchar(255) DEFAULT NULL,
  `aik_biglogo` varchar(255) DEFAULT NULL,
  `aik_onelogo` varchar(255) DEFAULT NULL,
  `aik_twologo` varchar(255) DEFAULT NULL,
  `aik_wx_url` varchar(255) DEFAULT NULL,
  `aik_wx_token` varchar(255) DEFAULT NULL,
  `aik_wx_appid` varchar(255) DEFAULT NULL,
  `aik_wx_appsecret` varchar(255) DEFAULT NULL,
  `aik_wx_img` varchar(255) DEFAULT NULL,
  `aik_wx_reply` varchar(255) DEFAULT NULL,
  `aik_admin` varchar(255) DEFAULT NULL,
  `aik_admin_img` varchar(255) NOT NULL,
  `aik_admin_qq` varchar(32) NOT NULL,
  `aik_pw` varchar(255) DEFAULT NULL,
  `aik_user_open` int(11) DEFAULT NULL,
  `aik_fbjiexi1` varchar(255) DEFAULT NULL,
  `aik_fbjiexi2` varchar(255) DEFAULT NULL,
  `aik_jiexi6` varchar(255) DEFAULT NULL,
  `aik_qqappid` varchar(32) DEFAULT NULL,
  `aik_qqappkey` varchar(255) DEFAULT NULL,
  `aik_qqlogin` int(11) DEFAULT NULL,
  `aik_fresh_on` int(11) DEFAULT NULL,
  `aik_fresh_url` varchar(255) DEFAULT NULL,
  `aik_rec_on` int(11) DEFAULT NULL,
  `aik_login_play` int(12) NOT NULL,
  `aik_tort` varchar(255) DEFAULT NULL,
  `aik_zhibo` int(11) DEFAULT NULL,
  `aik_email_server` varchar(255) DEFAULT NULL,
  `aik_email_port` int(11) DEFAULT NULL,
  `aik_email_add` varchar(255) DEFAULT NULL,
  `aik_email_user` varchar(255) DEFAULT NULL,
  `aik_email_pw` varchar(255) DEFAULT NULL,
  `aik_email_inbox` varchar(255) DEFAULT NULL,
  `aik_pay_name` varchar(255) DEFAULT NULL,
  `aik_pay_id` int(255) DEFAULT NULL,
  `aik_pay_key` varchar(255) DEFAULT NULL,
  `aik_pay_url` varchar(255) DEFAULT NULL,
  `aik_pay_open` int(11) DEFAULT NULL,
  `aik_qqurl` varchar(255) DEFAULT NULL,
  `aik_user_intra` int(32) DEFAULT NULL,
  `aik_user_affint` int(32) DEFAULT NULL,
  `aik_user_initint` int(32) DEFAULT NULL,
  `aik_buycard` varchar(255) DEFAULT NULL,
  `aik_union_open` int(11) DEFAULT NULL,
  `aik_union_dtk` varchar(255) NOT NULL,
  `aik_union_dtkid` varchar(255) NOT NULL,
  `aik_union_jtt` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `aikcms_basic`
--

INSERT INTO `aikcms_basic` (`id`, `aik_name`, `aik_domain`, `aik_title`, `aik_keywords`, `aik_desc`, `aik_shouquan`, `aik_template`, `aik_mtemplate`, `aik_icp`, `aik_tongji`, `aik_changyan`, `aik_jiexi1`, `aik_jiexi2`, `aik_jiexi3`, `aik_jiexi4`, `aik_jiexi5`, `aik_cache`, `aik_weijing`, `aik_onlym`, `aik_postercome`, `aik_wxsk`, `aik_zfbsk`, `aik_wxgzh`, `aik_qqjq`, `aik_appxz`, `aik_biglogo`, `aik_onelogo`, `aik_twologo`, `aik_wx_url`, `aik_wx_token`, `aik_wx_appid`, `aik_wx_appsecret`, `aik_wx_img`, `aik_wx_reply`, `aik_admin`, `aik_admin_img`, `aik_admin_qq`, `aik_pw`, `aik_user_open`, `aik_fbjiexi1`, `aik_fbjiexi2`, `aik_jiexi6`, `aik_qqappid`, `aik_qqappkey`, `aik_qqlogin`, `aik_fresh_on`, `aik_fresh_url`, `aik_rec_on`, `aik_login_play`, `aik_tort`, `aik_zhibo`, `aik_email_server`, `aik_email_port`, `aik_email_add`, `aik_email_user`, `aik_email_pw`, `aik_email_inbox`, `aik_pay_name`, `aik_pay_id`, `aik_pay_key`, `aik_pay_url`, `aik_pay_open`, `aik_qqurl`, `aik_user_intra`, `aik_user_affint`, `aik_user_initint`, `aik_buycard`, `aik_union_open`, `aik_union_dtk`, `aik_union_dtkid`, `aik_union_jtt`) VALUES
(1, 'AikCMS - 演示站', 'http://localhost/', '﻿爱客影院 - 海量高清VIP视频免费观看', '爱客影院,电视直播网站,零八影院快播,高清云影视,云点播,免费看视频,湖南卫视直播,80电影网,最新电影天堂免费在线观看', '﻿爱客影院,热剧快播,最好看的剧情片尽在﻿爱客影院,高清云影视免费为大家提供最新最全的免费电影，电视剧，综艺，动漫无广告在线云点播，以及电视直播', '', 'default', 'default', '国ICP备18888888号', '', '', 'http://130ak.cn/?v= ', 'http://130ak.cn/?v= ', 'http://130ak.cn/?v= ', 'http://130ak.cn/?v= ', 'http://130ak.cn/?v= ', '1', '0', '0', '3', '/upload/wx.png', '/upload/zfb.png', '/http://jx.kk25.xyz/images/code.png', '/upload/1552978739620.png', '/upload/qrcode_for_gh_135f3158a545_258.jpg', '/upload/logo.png', '/upload/sologo.png', '', '1', 'weixin', 'wx07cf4ae335bbff12', '23434324234', '', '微信最大在线观看电影平台,最新院线大片免费看,输入电影【关键字】即可获取最新电影,上万部院线大片资源,可能是微信上最有温度的电影平台', 'admin', '/upload/admin.jpg', '88888888', 'e10adc3949ba59abbe56e057f20f883e', 1, 'http://130ak.cn/?v= ', '', 'http://130ak.cn/?v= ', '1455112844', '4b28abd4f5812d4aad3313b13b5ceb45', 1, 2, 'http://www.123ku.com/inc/feifei3.4/', 1, 0, '醉玲珑#无证之罪#权力的游戏第七季#奇葩大会第2季#相爱的七种设计#我的恋爱时代', 2, 'smtp.163.com', 25, '', '', '', '', '1', 10005422, 'b5fd07acc172aa6658f2613b7c592e9382606227f719b8b0', 'https://interface.lcardy.com/Online_Banking_interface', 1, '4b28abd4f5812d4aad3313b13b5ceb45', 10, 10, 10, 'http://qqds.xchwl.cn', 3, 'http://www.domain.com', '957625', 'http://www.domain.com');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_card`
--

CREATE TABLE IF NOT EXISTS `aikcms_card` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aik_card` varchar(255) NOT NULL DEFAULT '',
  `aik_card_group` varchar(8) NOT NULL DEFAULT '' COMMENT '分类',
  `aik_card_int` varchar(11) NOT NULL DEFAULT '' COMMENT '天数',
  `aik_card_time` varchar(255) NOT NULL DEFAULT '0',
  `aik_card_user` varchar(255) NOT NULL DEFAULT '',
  `aik_card_utime` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `c_used` (`aik_card_time`),
  KEY `c_sale` (`aik_card_user`),
  KEY `c_user` (`aik_card_utime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_colgroup`
--

CREATE TABLE IF NOT EXISTS `aikcms_colgroup` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aik_colgroup_name` varchar(255) NOT NULL,
  `aik_colgroup_fburl` varchar(255) NOT NULL,
  `aik_colgroup_cjurl` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`aik_colgroup_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `aikcms_colgroup`
--

INSERT INTO `aikcms_colgroup` (`id`, `aik_colgroup_name`, `aik_colgroup_fburl`, `aik_colgroup_cjurl`) VALUES
(1, '资源站（一）', '1550582756zyzfb.php', '1550582756zyzcj.php');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_colrule`
--

CREATE TABLE IF NOT EXISTS `aikcms_colrule` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `aik_col_name` varchar(255) NOT NULL DEFAULT '',
  `aik_col_url` varchar(255) NOT NULL DEFAULT '',
  `aik_col_urlstart` varchar(32) NOT NULL DEFAULT '',
  `aik_col_urlend` varchar(32) NOT NULL DEFAULT '',
  `aik_col_videogroup` varchar(32) NOT NULL DEFAULT '',
  `aik_col_usergroup` varchar(32) NOT NULL,
  `aik_col_rule` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `aikcms_colrule`
--

INSERT INTO `aikcms_colrule` (`id`, `aik_col_name`, `aik_col_url`, `aik_col_urlstart`, `aik_col_urlend`, `aik_col_videogroup`, `aik_col_usergroup`, `aik_col_rule`) VALUES
(1, 'OK资源网-动作片', 'http://okokzy.cc/?m=vod-type-id-5.html', '1', '1', '1', '0', '1550582756zyzcj.php'),
(2, 'OK资源网-喜剧片', 'http://www.okzyw.com/?m=vod-type-id-6.html', '1', '2', '2', '0', '1550582756zyzcj.php');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_gbook`
--

CREATE TABLE IF NOT EXISTS `aikcms_gbook` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aik_gbook_content` text NOT NULL,
  `aik_gbook_time` varchar(255) NOT NULL,
  `aik_gbook_reply` int(11) NOT NULL DEFAULT '0',
  `aik_gbook_userid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_link`
--

CREATE TABLE IF NOT EXISTS `aikcms_link` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `aik_link_name` varchar(64) NOT NULL DEFAULT '',
  `aik_link_img` varchar(255) NOT NULL DEFAULT '',
  `aik_link_url` varchar(255) NOT NULL DEFAULT '',
  `aik_link_qq` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `aikcms_link`
--

INSERT INTO `aikcms_link` (`id`, `aik_link_name`, `aik_link_img`, `aik_link_url`, `aik_link_qq`) VALUES
(1, '网络爱好者', '', 'http://www.domain.com', '');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_nav`
--

CREATE TABLE IF NOT EXISTS `aikcms_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_nav_name` varchar(255) DEFAULT NULL,
  `aik_nav_color` varchar(32) NOT NULL,
  `aik_nav_papa` int(11) NOT NULL DEFAULT '0',
  `aik_nav_url` varchar(255) DEFAULT NULL,
  `aik_nav_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `aikcms_nav`
--

INSERT INTO `aikcms_nav` (`id`, `aik_nav_name`, `aik_nav_color`, `aik_nav_papa`, `aik_nav_url`, `aik_nav_id`) VALUES
(1, '爱客券券', '#FF0000', 0, '', 0),
(2, '淘宝内部券', '', 1, 'http://qqds.xchwl.cn/', 0),
(3, '京东优惠券', '', 1, 'http://qqds.xchwl.cn/', 0),
(4, '直播', '', 0, '/index.php?mode=zhibo', 0),
(5, '求片留言', '', 0, '/index.php?mode=gbook', 0);

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_notice`
--

CREATE TABLE IF NOT EXISTS `aikcms_notice` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aik_notice_title` varchar(255) NOT NULL,
  `aik_notice_url` varchar(255) NOT NULL,
  `aik_notice_time` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`aik_notice_title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `aikcms_notice`
--

INSERT INTO `aikcms_notice` (`id`, `aik_notice_title`, `aik_notice_url`, `aik_notice_time`) VALUES
(1, 'QQ代刷网', 'http://qqds.xchwl.cn/', '26211899');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_poster`
--

CREATE TABLE IF NOT EXISTS `aikcms_poster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_hd_name` varchar(255) DEFAULT NULL,
  `aik_hd_img` varchar(255) DEFAULT NULL,
  `aik_hd_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `aikcms_poster`
--

INSERT INTO `aikcms_poster` (`id`, `aik_hd_name`, `aik_hd_img`, `aik_hd_link`) VALUES
(1, '新人领60元优惠券', '/upload/1.png', 'http://qqds.xchwl.cn/');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_user`
--

CREATE TABLE IF NOT EXISTS `aikcms_user` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `aik_user_img` varchar(255) NOT NULL DEFAULT '/upload/default/default.gif',
  `aik_user_name` varchar(255) NOT NULL DEFAULT '',
  `aik_user_alias` varchar(255) NOT NULL DEFAULT '',
  `aik_user_pw` varchar(255) NOT NULL DEFAULT '',
  `aik_user_group` varchar(255) NOT NULL DEFAULT '0',
  `aik_user_int` varchar(32) NOT NULL DEFAULT '0',
  `aik_user_time` varchar(255) NOT NULL DEFAULT '',
  `aik_user_qq` varchar(255) NOT NULL DEFAULT '',
  `aik_user_on` tinyint(1) NOT NULL DEFAULT '1',
  `aik_user_nick` varchar(255) NOT NULL DEFAULT '',
  `aik_user_qqlogin` int(11) NOT NULL DEFAULT '0',
  `aik_user_allmoney` varchar(32) NOT NULL DEFAULT '0',
  `aik_user_usedmoney` varchar(32) NOT NULL DEFAULT '0',
  `aik_user_groupend` char(255) NOT NULL DEFAULT '0',
  `aik_user_affid` varchar(11) NOT NULL DEFAULT '',
  `aik_user_ip` varchar(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `u_group` (`aik_user_allmoney`),
  KEY `u_status` (`aik_user_qq`),
  KEY `u_flag` (`aik_user_on`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_usergroup`
--

CREATE TABLE IF NOT EXISTS `aikcms_usergroup` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `aik_usergroup_name` varchar(255) NOT NULL DEFAULT '',
  `aik_usergroup_img` varchar(255) NOT NULL,
  `aik_usergroup_price` varchar(255) NOT NULL DEFAULT '',
  `aik_usergroup_length` varchar(255) NOT NULL DEFAULT '',
  `aik_usergroup_level` int(11) NOT NULL,
  `aik_usergroup_rem` varchar(255) NOT NULL DEFAULT '',
  `aik_usergroup_open` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `aikcms_usergroup`
--

INSERT INTO `aikcms_usergroup` (`id`, `aik_usergroup_name`, `aik_usergroup_img`, `aik_usergroup_price`, `aik_usergroup_length`, `aik_usergroup_level`, `aik_usergroup_rem`, `aik_usergroup_open`) VALUES
(1, '赞助会员', '/upload/21.gif', '8.8', '31', 0, '', 0),
(2, '年费会员', '/upload/22.gif', '88.8', '366', 0, '', 0),
(3, '永久会员', '/upload/24.gif', '888.8', '0', 0, '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_user_fav`
--

CREATE TABLE IF NOT EXISTS `aikcms_user_fav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_fav_name` varchar(255) DEFAULT NULL,
  `aik_fav_url` varchar(255) DEFAULT NULL,
  `aik_fav_userid` int(11) DEFAULT NULL,
  `aik_fav_time` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_user_history`
--

CREATE TABLE IF NOT EXISTS `aikcms_user_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_history_name` varchar(255) DEFAULT NULL,
  `aik_history_url` varchar(255) DEFAULT NULL,
  `aik_history_userid` int(11) DEFAULT NULL,
  `aik_history_time` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_user_order`
--

CREATE TABLE IF NOT EXISTS `aikcms_user_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_order` varchar(255) NOT NULL,
  `aik_order_name` varchar(255) DEFAULT NULL,
  `aik_order_videoid` int(64) NOT NULL,
  `aik_order_videourl` varchar(255) NOT NULL,
  `aik_order_price` varchar(255) DEFAULT NULL,
  `aik_order_userid` int(11) DEFAULT NULL,
  `aik_order_time` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_user_pay`
--

CREATE TABLE IF NOT EXISTS `aikcms_user_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aik_pay_order` varchar(255) DEFAULT NULL,
  `aik_pay_num` varchar(255) DEFAULT NULL,
  `aik_pay_mode` varchar(32) NOT NULL,
  `aik_pay_userid` int(11) DEFAULT NULL,
  `aik_pay_state` int(11) NOT NULL,
  `aik_pay_time` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_video`
--

CREATE TABLE IF NOT EXISTS `aikcms_video` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aik_video_name` varchar(255) NOT NULL DEFAULT '',
  `aik_video_img` varchar(255) NOT NULL DEFAULT '',
  `aik_video_seo` varchar(255) DEFAULT NULL,
  `aik_video_group` varchar(11) DEFAULT '',
  `aik_video_pay` varchar(11) NOT NULL DEFAULT '',
  `aik_video_int` varchar(255) DEFAULT '0',
  `aik_video_usergroup` varchar(255) DEFAULT NULL,
  `aik_video_level` varchar(11) DEFAULT '0',
  `aik_video_jiexi` varchar(255) DEFAULT NULL,
  `aik_video_url` mediumtext,
  `aik_video_time` varchar(255) DEFAULT NULL,
  `aik_video_remarks` varchar(255) DEFAULT NULL,
  `aik_video_col` varchar(255) DEFAULT NULL,
  `aik_video_zylink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `d_letter` (`aik_video_group`),
  KEY `d_name` (`aik_video_name`),
  KEY `d_enname` (`aik_video_seo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- 转存表中的数据 `aikcms_video`
--

INSERT INTO `aikcms_video` (`id`, `aik_video_name`, `aik_video_img`, `aik_video_seo`, `aik_video_group`, `aik_video_pay`, `aik_video_int`, `aik_video_usergroup`, `aik_video_level`, `aik_video_jiexi`, `aik_video_url`, `aik_video_time`, `aik_video_remarks`, `aik_video_col`, `aik_video_zylink`) VALUES
(1, '切尔诺贝利·禁区电影版', 'https://mahuapic.com/upload/vod/2020-03-11/202003111583934547.png', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$https://mhkuaibo.com/share/CLlPIMhyQ6bPLZtp\r\nHD高清$https://mhkuaibo.com/20200311/dsTShzIT/index.m3u8\r\nHD高清$http://xz3-9.okzyxz.com/20190319/13459_1316b451/吃鸡战场.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30522.html'),
(2, '绝路反击HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155295671812.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://56.com-t-56.com/share/8ff3fdef6f5144f50eb2a83cd34baa5d\r\nHD高清$http://56.com-t-56.com/20190318/9555_15b39d11/index.m3u8\r\nHD高清$http://xunlei11.okzyxz.com/20190318/9555_15b39d11/绝路反击.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30517.html'),
(3, '最后的兵团HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15529326731.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://youku.com-t-youku.com/share/217dade2ab7db91d12f1bca7b0cd4c82\r\nHD高清$http://youku.com-t-youku.com/20190318/9605_95493150/index.m3u8\r\nHD高清$http://xz3-7.okzyxz.com/20190318/9605_95493150/最后的兵团.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30504.html'),
(4, '水手服与机关枪：毕业HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15528927460.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://56.com-t-56.com/share/4e668929edb3bf915e1a3a9d96c3c97e\r\nHD高清$http://56.com-t-56.com/20190317/9364_efc791ec/index.m3u8\r\nHD高清$http://xunlei11.okzyxz.com/20190317/9364_efc791ec/水手服与机关枪：毕业.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30463.html'),
(5, '水手服与机关枪1981HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15528927409.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://56.com-t-56.com/share/c535e3a7f97daf1c4b1eb03cc8e31623\r\nHD高清$http://56.com-t-56.com/20190317/9357_1b46952e/index.m3u8\r\nHD高清$http://xunlei11.okzyxz.com/20190317/9357_1b46952e/水手服与机关枪.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30462.html'),
(6, '永生之地HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15528884830.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD中字$http://bilibili.com-h-bilibili.com/share/518fc66deea9d064d0a92eb73e4ea61b\r\nHD中字$http://bilibili.com-h-bilibili.com/20190318/8135_331563ac/index.m3u8\r\nHD中字$http://xz3-8.okzyxz.com/20190318/8135_331563ac/永生之地.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30451.html'),
(7, '危险元素HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15528402750.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD中字$http://bilibili.com-h-bilibili.com/share/8677065f187e98d8beacdc700e49f6ef\r\nHD中字$http://bilibili.com-h-bilibili.com/20190318/8132_f82e3c7d/index.m3u8\r\nHD中字$http://xz3-8.okzyxz.com/20190318/8132_f82e3c7d/危险元素.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30446.html'),
(8, '画壁HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15528346866.jpg', NULL, '1', '', '0', '0', '0', NULL, '粤语$http://youku.com-t-youku.com/share/2ed0828621535a2a85a8f8e3388080d2\r\n国语$http://youku.com-t-youku.com/share/496bd33584d955e3913f1a3e82bb2f2d\r\n粤语$http://youku.com-t-youku.com/20190317/9104_3e32748e/index.m3u8\r\n国语$http://youku.com-t-youku.com/20190317/9103_8f8e76f4/index.m3u8\r\n粤语$http://xz3-7.okzyxz.com/20190317/9104_3e32748e/画壁粤语.mp4\r\n国语$http://xz3-7.okzyxz.com/20190317/9103_8f8e76f4/画壁国语.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30442.html'),
(9, '图书馆战争2：最后的任务HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527482182.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://56.com-t-56.com/share/0bc10d8a74dbafbf242e30433e83aa56\r\nHD高清$http://56.com-t-56.com/20190317/9361_2e286b63/index.m3u8\r\nHD高清$http://xunlei11.okzyxz.com/20190317/9361_2e286b63/图书馆战争2最后的任务.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30387.html'),
(10, '图书馆战争：记忆之书HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527482213.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://56.com-t-56.com/share/8c5b18cbdfd35320a10729d3aaf1343b\r\nHD高清$http://56.com-t-56.com/20190317/9360_0dd0bb5d/index.m3u8\r\nHD高清$http://xunlei11.okzyxz.com/20190317/9360_0dd0bb5d/图书馆战争：记忆之书.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30388.html'),
(11, '图书馆战争真人版HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527482151.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://56.com-t-56.com/share/cf34645d98a7630e2bcca98b3e29c8f2\r\nHD高清$http://56.com-t-56.com/20190317/9358_3a22fc74/index.m3u8\r\nHD高清$http://xunlei11.okzyxz.com/20190317/9358_3a22fc74/图书馆战争.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30386.html'),
(12, '敢死七镖客HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527508560.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD中字$http://bilibili.com-h-bilibili.com/share/a34c46916c53099c8038cff641c9b127\r\nHD中字$http://bilibili.com-h-bilibili.com/20190316/8125_e395fc71/index.m3u8\r\nHD中字$http://xz3-8.okzyxz.com/20190316/8125_e395fc71/敢死七镖客.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30366.html'),
(13, '伸冤人HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527489420.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://youku.com-t-youku.com/share/3de6a598010e6866124ddfa12d3d35cb\r\nHD高清$http://youku.com-t-youku.com/20190316/8843_383d800e/index.m3u8\r\nHD高清$http://xz3-7.okzyxz.com/20190316/8843_383d800e/伸冤人.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30393.html'),
(14, '火拼油尖区HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527487231.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://youku.com-t-youku.com/share/6e88ec1459f337d5bea6353f8bff8026\r\nHD高清$http://youku.com-t-youku.com/20190316/8841_924124df/index.m3u8\r\nHD高清$http://xz3-7.okzyxz.com/20190316/8841_924124df/火拼油尖区.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30391.html'),
(15, '最终正义HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527468675.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://youku.com-t-youku.com/share/d0f5edad9ac19abed9e235c0fe0aa59f\r\nHD高清$http://youku.com-t-youku.com/20190315/8838_53852561/index.m3u8\r\nHD高清$http://xz3-7.okzyxz.com/20190315/8838_53852561/最终正义.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30379.html'),
(16, '复仇者格林：战事再起HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527421460.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD中字$http://bilibili.com-h-bilibili.com/share/15c71b874531f45bba372bfc35e9b8cf\r\nHD中字$http://bilibili.com-h-bilibili.com/20190316/8123_cd8a9648/index.m3u8\r\nHD中字$http://xz3-8.okzyxz.com/20190316/8123_cd8a9648/复仇者格林：战事再起HD中字.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30349.html'),
(17, '缘断仰光桥HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15526496840.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD中字$http://bilibili.com-h-bilibili.com/share/b20fa060328b0cdf51b464ee37efe182\r\nHD中字$http://bilibili.com-h-bilibili.com/20190315/8114_fe4816fa/index.m3u8\r\nHD中字$http://xz3-8.okzyxz.com/20190315/8114_fe4816fa/缘断仰光桥1080P.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30298.html'),
(18, '千局1HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15527309731.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/9b8fe41d7a9a359029570f1d2ef42440\r\nHD高清$http://baidu.com-l-baidu.com/20190316/13368_f776edc9/index.m3u8\r\nHD高清$http://xz3-9.okzyxz.com/20190316/13368_f776edc9/千局1.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30335.html'),
(19, '筋疲力尽HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15526452470.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD中字$http://bilibili.com-h-bilibili.com/share/b6417f112bd27848533e54885b66c288\r\nHD中字$http://bilibili.com-h-bilibili.com/20190315/8113_f5ef8d95/index.m3u8\r\nHD中字$http://xz3-8.okzyxz.com/20190315/8113_f5ef8d95/筋疲力尽HD中字.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30297.html'),
(20, '喋血天使HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903141552544854.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/4de9b7822e0de81fc734bc5689ab6f03\r\nHD高清$http://baidu.com-l-baidu.com/20190314/13098_c010f1ba/index.m3u8\r\nHD高清$http://xunlei5.okzyxz.com/20190314/13098_c010f1ba/喋血天使.Dust.Angel.2019.HD720P.X264.AAC.Korean.CHT.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30258.html'),
(21, '风雨双流星HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903141552535000.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/76185584223b2f7b9f3a91a2f9913135\r\nHD高清$http://baidu.com-l-baidu.com/20190314/13010_78dd2859/index.m3u8\r\nHD高清$http://xunlei5.okzyxz.com/20190314/13010_78dd2859/风雨双流星-国语高清.flv.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30197.html'),
(22, '海上男孩HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903141552528826.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://qq.com-l-qq.com/share/af086cdab7954f11a518e3af68dc2fce\r\nHD高清$http://qq.com-l-qq.com/20190314/9168_0366ad55/index.m3u8\r\nHD高清$http://xunlei2.okzyxz.com/20190314/9168_0366ad55/海上男孩-国语高清.flv.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30178.html'),
(23, '三方国界HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903131552485949.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/97f58cc60361f36cb40942c5c9a9e029\r\nHD高清$http://baidu.com-l-baidu.com/20190313/12986_cf303fa6/index.m3u8\r\nHD高清$http://xunlei5.okzyxz.com/20190313/12986_cf303fa6/Triple.Frontier.2019.720p.NF.WEB-DL.DDP5.1.x264-NTG.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30165.html'),
(24, '勇闯毒龙潭HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155239006517.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/cb804af641d900ffe033193d2b7c4a84\r\nHD高清$http://sina.com-h-sina.com/20190311/30136_c57bf77e/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30136_c57bf77e/勇闯毒龙潭.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30134.html'),
(25, '英雄重英雄HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155239006216.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/a00ba776735f6e27e0619d46a07be9d3\r\nHD高清$http://sina.com-h-sina.com/20190311/30135_e868dc52/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30135_e868dc52/英雄重英雄.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30133.html'),
(26, '特警90之亡命天涯HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155239006015.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/c980fea9ff9c4bb4b175e2708d05e417\r\nHD高清$http://sina.com-h-sina.com/20190311/30134_513060fc/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30134_513060fc/特警90之亡命天涯.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30132.html'),
(27, '特警90之明日天涯HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155239005614.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/8c15857537ac1c1216ffb4d2fb1d6805\r\nHD高清$http://sina.com-h-sina.com/20190311/30133_a2b8f4b7/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30133_a2b8f4b7/特警90之明日天涯.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30131.html'),
(28, '特警90HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155239005313.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/56c25287be94f28752503a1cb341c896\r\nHD高清$http://sina.com-h-sina.com/20190311/30132_8435e8f8/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30132_8435e8f8/特警90.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30130.html'),
(29, '死里逃生2003HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903121552389388.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/433db795566d9f51a72086333588e655\r\nHD高清$http://sina.com-h-sina.com/20190311/30130_bf9882e1/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30130_bf9882e1/死里逃生.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30127.html'),
(30, '难民营风暴HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15523900378.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/b2e65e738c327d1a8c3c27092d00b6c1\r\nHD高清$http://sina.com-h-sina.com/20190312/30657_38d24345/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190312/30657_38d24345/难民营风暴.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30123.html'),
(31, '剑客与灵童HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15523900347.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/c2411c0857a5002c42d2b16457deba2f\r\nHD高清$http://sina.com-h-sina.com/20190311/30123_3b6e2eeb/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190311/30123_3b6e2eeb/剑客与灵童.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30120.html'),
(32, '命之途HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903121552388195.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/5eed2924bb4dd16357ee68f6c160befa\r\nHD高清$http://baidu.com-l-baidu.com/20190312/12894_441eaa0e/index.m3u8\r\nHD高清$http://xunlei5.okzyxz.com/20190312/12894_441eaa0e/命之途.2019.HD720P.X264.AAC.Mandarin.CHS.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30112.html'),
(33, '最后一人BD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903121552340369.jpg', NULL, '1', '', '0', '0', '0', NULL, 'BD超清$http://qq.com-l-qq.com/share/2ed0828621535a2a85a8f8e3388080d2\r\nBD超清$http://qq.com-l-qq.com/20190312/9104_8ee8ea25/index.m3u8\r\nBD超清$http://xunlei2.okzyxz.com/20190312/9104_8ee8ea25/最后一人.Numb.at.the.Edge.of.the.End.2018.BD1080P.X264.AAC.English.CHS-ENG.mp4', '1552979080', 'BD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30063.html'),
(34, '死亡陷阱HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903121552340015.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://qq.com-l-qq.com/share/abceedf5017915685f379075f00a5ccd\r\nHD高清$http://qq.com-l-qq.com/20190312/9101_040af8d8/index.m3u8\r\nHD高清$http://xunlei2.okzyxz.com/20190312/9101_040af8d8/死亡陷阱.The.DMZ.2017.HD720P.X264.AAC.Korean.CHT.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30060.html'),
(35, '逃出亚卡拉HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15523849702.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://qq.com-l-qq.com/share/a44ba9086b2b83ccf2baf7c678723449\r\nHD高清$http://qq.com-l-qq.com/20190311/9083_f4a0697b/index.m3u8\r\nHD高清$http://xunlei2.okzyxz.com/20190311/9083_f4a0697b/逃出亚卡拉-英语720P.flv.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30046.html'),
(36, '最危险的游戏HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903111552288575.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://pptv.com-h-pptv.com/share/8248f16fa738b0bfe6013edf69d873bf\r\nHD高清$http://pptv.com-h-pptv.com/20190311/15946_3b80aba4/index.m3u8\r\nHD高清$http://xunlei3.okzyxz.com/20190311/15946_3b80aba4/最危险的游戏.The.Most.Dangerous.Game.2017.HD720P.X264.AAC.English.CHT.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30017.html'),
(37, '死亡陷阱HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903111552284627.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/237949cceaabe986ac5c1cd65cef13ff\r\nHD高清$http://baidu.com-l-baidu.com/20190311/12850_acc58c59/index.m3u8\r\nHD高清$http://xunlei5.okzyxz.com/20190311/12850_acc58c59/死亡陷阱.The.DMZ.2017.HD720P.X264.AAC.Korean.CHT.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30011.html'),
(38, '少女四大名捕HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/201903111552284308.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://baidu.com-l-baidu.com/share/186f8f4eba86cca7bdf06db31f155a15\r\nHD高清$http://baidu.com-l-baidu.com/20190311/12852_34df4150/index.m3u8\r\nHD高清$http://xunlei5.okzyxz.com/20190311/12852_34df4150/少女四大名捕.2019.HD720P.X264.AAC.Mandarin.CHS.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30008.html'),
(39, '黑夜传说HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/155219886917.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://youku.com-t-youku.com/share/debe236f3c30658190a8fe363a2b5cc0\r\nHD高清$http://youku.com-t-youku.com/20190310/8151_2f67243f/index.m3u8\r\nHD高清$http://xunlei9.okzyxz.com/20190310/8151_2f67243f/黑夜传说2003.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-30002.html'),
(40, '神探蒲松龄HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15521988820.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/41e56f14c6c580912b67c91133643a54\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190310/21967_c5662571/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190310/21967_c5662571/神探蒲松龄.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-28360.html'),
(41, '王牌霸王花HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15521179053.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/36e0c72e6254a2992a95c193194f52a5\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190309/21964_776b264e/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190309/21964_776b264e/王牌霸王花.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29980.html'),
(42, '血色皇冠HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15521161816.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/72fb5a1bbd1cda5a11251ba1d220672e\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190309/21954_e46526a9/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190309/21954_e46526a9/血色皇冠 Bloodstone (1988).mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29971.html'),
(43, '利斧巨人:保罗班扬的愤怒HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15521161600.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/5478c9cd79f4e04b6a2f6e2b4ab9f513\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190309/21949_a2d23be2/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190309/21949_a2d23be2/利斧巨人：保罗班扬的愤怒.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29966.html'),
(44, '蜀山：新蜀山剑侠HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15520981586.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://sina.com-h-sina.com/share/68da88f6136bd6e456811e4a1f941ac0\r\nHD高清$http://sina.com-h-sina.com/20190308/29888_1a38a408/index.m3u8\r\nHD高清$http://xunlei7.okzyxz.com/20190308/29888_1a38a408/新蜀山剑侠1983.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29960.html'),
(45, '刺客荣耀·荆轲HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15520178430.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/8625af362985c33b7525678f3536b1b1\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190308/21910_7aec7ce3/index.m3u8\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190308/21910_7aec7ce3/index.m3u8', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-12146.html'),
(46, '天下第一镖局HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15520178471.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/d84f9ebccf1b062f6dce85d56fe09420\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190308/21914_c710d891/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190308/21914_c710d891/天下第一镖局The Bravest Escort Group 2018.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-18846.html'),
(47, '七剑下天山之修罗眼HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15520178500.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/f5c981c11d76faa6f95e9dfad16e3eb9\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190308/21911_26922f26/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190308/21911_26922f26/七剑下天山之修罗眼 The Seven Swords.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29906.html'),
(48, '大内高手HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15519354222.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/4ddb6f74a1c9119602bc9053b5f2b4f3\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190307/21873_d43fcd07/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190307/21873_d43fcd07/大内高手The.Imperial.Swordsman.1972.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29869.html'),
(49, '女侠黑蝴蝶HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15519354160.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/460eb45f1f9b67a58c96a73be551f37b\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190307/21871_e8704a22/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190307/21871_e8704a22/黑蝴蝶Black.Butterfly.1968.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29867.html'),
(50, '龙虎会风云HD', 'http://pic.china-gif.com/pic/upload/vod/2019-03/15519354120.jpg', NULL, '1', '', '0', '0', '0', NULL, 'HD高清$http://iqiyi.com-l-iqiyi.com/share/3ac44bdb7fb0bbd5c8b3f4db0218d05e\r\nHD高清$http://iqiyi.com-l-iqiyi.com/20190307/21870_a143c803/index.m3u8\r\nHD高清$http://xunlei.okzyxz.com/20190307/21870_a143c803/龙虎会风云Heroes.of.Sung.1973.mp4', '1552979080', 'HD', '1550582756zyzcj.php', 'http://okokzy.cc/?m=vod-detail-id-29866.html');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_videogroup`
--

CREATE TABLE IF NOT EXISTS `aikcms_videogroup` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aik_videogroup_name` varchar(255) NOT NULL,
  `aik_videogroup_usergroup` varchar(68) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`aik_videogroup_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `aikcms_videogroup`
--

INSERT INTO `aikcms_videogroup` (`id`, `aik_videogroup_name`, `aik_videogroup_usergroup`) VALUES
(1, '动作片', '0'),
(2, '喜剧片', '0'),
(3, '会员专属', '1,2,3');

-- --------------------------------------------------------

--
-- 表的结构 `aikcms_zhibo`
--

CREATE TABLE IF NOT EXISTS `aikcms_zhibo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aik_zhibo_name` varchar(255) NOT NULL,
  `aik_zhibo_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`aik_zhibo_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- 转存表中的数据 `aikcms_zhibo`
--

INSERT INTO `aikcms_zhibo` (`id`, `aik_zhibo_name`, `aik_zhibo_url`) VALUES
(1, 'CCTV-1高清', 'http://ivi.bupt.edu.cn/player.html?channel=cctv1hd'),
(2, 'CCTV-3高清', 'http://ivi.bupt.edu.cn/player.html?channel=cctv3hd'),
(3, 'CCTV-5+高清', 'http://ivi.bupt.edu.cn/player.html?channel=cctv5phd'),
(4, 'CCTV-6高清', 'http://ivi.bupt.edu.cn/player.html?channel=cctv6hd'),
(5, 'CCTV-8高清', 'http://ivi.bupt.edu.cn/player.html?channel=cctv8hd'),
(6, 'CHC高清电影', 'http://ivi.bupt.edu.cn/player.html?channel=chchd'),
(7, '北京卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=btv1hd'),
(8, '北京文艺高清', 'http://ivi.bupt.edu.cn/player.html?channel=btv2hd'),
(9, '北京纪实高清', 'http://ivi.bupt.edu.cn/player.html?channel=btv11hd'),
(10, '湖南卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=hunanhd'),
(11, '浙江卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=zjhd'),
(12, '江苏卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=jshd'),
(13, '东方卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=dfhd'),
(14, '安徽卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=ahhd'),
(15, '黑龙江卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=hljhd'),
(16, '辽宁卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=lnhd'),
(17, '深圳卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=szhd'),
(18, '广东卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=gdhd'),
(19, '天津卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=tjhd'),
(20, '湖北卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=hbhd'),
(21, '山东卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=sdhd'),
(22, '重庆卫视高清', 'http://ivi.bupt.edu.cn/player.html?channel=cqhd'),
(23, 'CCTV-1综合', 'http://ivi.bupt.edu.cn/player.html?channel=cctv1'),
(24, 'CCTV-2财经', 'http://ivi.bupt.edu.cn/player.html?channel=cctv2'),
(25, 'CCTV-3综艺', 'http://ivi.bupt.edu.cn/player.html?channel=cctv3'),
(26, 'CCTV-4中文国际', 'http://ivi.bupt.edu.cn/player.html?channel=cctv4'),
(27, 'CCTV-6电影', 'http://ivi.bupt.edu.cn/player.html?channel=cctv6'),
(28, 'CCTV-7军事农业', 'http://ivi.bupt.edu.cn/player.html?channel=cctv7'),
(29, 'CCTV-8电视剧', 'http://ivi.bupt.edu.cn/player.html?channel=cctv8'),
(30, 'CCTV-9纪录', 'http://ivi.bupt.edu.cn/player.html?channel=cctv9'),
(31, 'CCTV-10科教', 'http://ivi.bupt.edu.cn/player.html?channel=cctv10'),
(32, 'CCTV-11戏曲', 'http://ivi.bupt.edu.cn/player.html?channel=cctv11'),
(33, 'CCTV-12社会与法', 'http://ivi.bupt.edu.cn/player.html?channel=cctv12'),
(34, 'CCTV-13新闻', 'http://ivi.bupt.edu.cn/player.html?channel=cctv13'),
(35, 'CCTV-14少儿', 'http://ivi.bupt.edu.cn/player.html?channel=cctv14'),
(36, 'CCTV-15音乐', 'http://ivi.bupt.edu.cn/player.html?channel=cctv15'),
(37, 'CCTV-NEWS', 'http://ivi.bupt.edu.cn/player.html?channel=cctv16'),
(38, '北京卫视', 'http://ivi.bupt.edu.cn/player.html?channel=btv1'),
(39, '北京文艺', 'http://ivi.bupt.edu.cn/player.html?channel=btv2'),
(40, '北京科教', 'http://ivi.bupt.edu.cn/player.html?channel=btv3'),
(41, '北京影视', 'http://ivi.bupt.edu.cn/player.html?channel=btv4'),
(42, '北京财经', 'http://ivi.bupt.edu.cn/player.html?channel=btv5'),
(43, '北京生活', 'http://ivi.bupt.edu.cn/player.html?channel=btv7'),
(44, '北京青年', 'http://ivi.bupt.edu.cn/player.html?channel=btv8'),
(45, '北京新闻', 'http://ivi.bupt.edu.cn/player.html?channel=btv9'),
(46, '北京卡酷少儿', 'http://ivi.bupt.edu.cn/player.html?channel=btv10'),
(47, '深圳卫视', 'http://ivi.bupt.edu.cn/player.html?channel=sztv'),
(48, '安徽卫视', 'http://ivi.bupt.edu.cn/player.html?channel=ahtv'),
(49, '河南卫视', 'http://ivi.bupt.edu.cn/player.html?channel=hntv'),
(50, '陕西卫视', 'http://ivi.bupt.edu.cn/player.html?channel=sxtv'),
(51, '吉林卫视', 'http://ivi.bupt.edu.cn/player.html?channel=jltv'),
(52, '广东卫视', 'http://ivi.bupt.edu.cn/player.html?channel=gdtv'),
(53, '山东卫视', 'http://ivi.bupt.edu.cn/player.html?channel=sdtv'),
(54, '湖北卫视', 'http://ivi.bupt.edu.cn/player.html?channel=hbtv'),
(55, '广西卫视', 'http://ivi.bupt.edu.cn/player.html?channel=gxtv'),
(56, '河北卫视', 'http://ivi.bupt.edu.cn/player.html?channel=hebtv'),
(57, '西藏卫视', 'http://ivi.bupt.edu.cn/player.html?channel=xztv'),
(58, '内蒙古卫视', 'http://ivi.bupt.edu.cn/player.html?channel=nmtv'),
(59, '青海卫视', 'http://ivi.bupt.edu.cn/player.html?channel=qhtv'),
(60, '四川卫视', 'http://ivi.bupt.edu.cn/player.html?channel=sctv'),
(61, '江苏卫视', 'http://ivi.bupt.edu.cn/player.html?channel=jstv'),
(62, '天津卫视', 'http://ivi.bupt.edu.cn/player.html?channel=tjtv'),
(63, '山西卫视', 'http://ivi.bupt.edu.cn/player.html?channel=sxrtv'),
(64, '辽宁卫视', 'http://ivi.bupt.edu.cn/player.html?channel=lntv'),
(65, '厦门卫视', 'http://ivi.bupt.edu.cn/player.html?channel=xmtv'),
(66, '新疆卫视', 'http://ivi.bupt.edu.cn/player.html?channel=xjtv'),
(67, '东方卫视', 'http://ivi.bupt.edu.cn/player.html?channel=dftv'),
(68, '黑龙江卫视', 'http://ivi.bupt.edu.cn/player.html?channel=hljtv'),
(69, '湖南卫视', 'http://ivi.bupt.edu.cn/player.html?channel=hunantv'),
(70, '云南卫视', 'http://ivi.bupt.edu.cn/player.html?channel=yntv'),
(71, '江西卫视', 'http://ivi.bupt.edu.cn/player.html?channel=jxtv'),
(72, '福建东南卫视', 'http://ivi.bupt.edu.cn/player.html?channel=dntv'),
(73, '浙江卫视', 'http://ivi.bupt.edu.cn/player.html?channel=zjtv'),
(74, '贵州卫视', 'http://ivi.bupt.edu.cn/player.html?channel=gztv'),
(75, '宁夏卫视', 'http://ivi.bupt.edu.cn/player.html?channel=nxtv'),
(76, '甘肃卫视', 'http://ivi.bupt.edu.cn/player.html?channel=gstv'),
(77, '重庆卫视', 'http://ivi.bupt.edu.cn/player.html?channel=cqtv'),
(78, '兵团卫视', 'http://ivi.bupt.edu.cn/player.html?channel=bttv'),
(79, '旅游卫视', 'http://ivi.bupt.edu.cn/player.html?channel=lytv');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
