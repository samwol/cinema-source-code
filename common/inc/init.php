<?php
if(!defined('AIKCMS')){
	show_error('Not in index');
}
if (!get_magic_quotes_gpc()){
	if (!empty($_GET)){
		$_GET = addslashes_deep($_GET);
	}
	if (!empty($_POST)){
		$_POST = addslashes_deep($_POST);
	}
	$_COOKIE   = addslashes_deep($_COOKIE);
	$_REQUEST  = addslashes_deep($_REQUEST);
}
extract($_GET);
extract($_POST);

function addslashes_deep($value){
	if (empty($value)){
		return $value;
	}else{
		return is_array($value) ? array_map('addslashes_deep', $value) : addslashes($value);
	}
}
function isMobile(){ 
    //检测访问的系统及版本、浏览器版本等信息
    $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
    $is_iphone = (strpos($agent, 'iphone')) ? true : false;
    $is_android = (strpos($agent, 'android')) ? true : false;
    if($is_android==1||$is_iphone==1)
    {
        return 1;//是手机

    }else
    {
        return 0;//不是手机
    }
}
function curl_get($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; U; Android 4.4.1; zh-cn; R815T Build/JOP40D) AppleWebKit/533.1 (KHTML, like Gecko)Version/4.0 MQQBrowser/4.5 Mobile Safari/533.1');
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $content = curl_exec($ch);
    curl_close($ch);
    return ($content);
}
function videogroup_select_list($_var_1)
{
	$_var_2 = 'select * from aikcms_videogroup order by id asc';
	$_var_3 = mysql_query($_var_2);
	while ($_var_4 = mysql_fetch_array($_var_3)) {
		$_var_5 = $_var_4['id'] == $_var_1 ? 'selected="selected"' : '';
		$_var_6 .= '<option value="' . $_var_4['id'] . '" ' . $_var_5 . '>' . $_var_4['aik_videogroup_name'] . '</option>';
	}
	return $_var_6;
}
function usergroup_select_list($_var_7)
{
	$_var_8 = 'select * from aikcms_usergroup order by id asc';
	$_var_9 = mysql_query($_var_8);
	while ($_var_10 = mysql_fetch_array($_var_9)) {
		$_var_11 = $_var_10['id'] == $_var_7 ? 'selected="selected"' : '';
		$_var_12 .= $_var_11.'<option value="' . $_var_10['id'] . '" ' . $_var_11 . '>' . $_var_10['aik_usergroup_name'] . '</option>';
	}
	return $_var_12;
}

function get_user_id($_var_13)
{
	$_var_14 = 'select * from aikcms_user where aik_user_name ="' . $_var_13 . '"';
	$_var_15 = mysql_query($_var_14);
	$_var_16 = mysql_fetch_array($_var_15);
	return $_var_16['id']; 
}

function get_user_img($_var_17)
{
	$_var_18 = 'select * from aikcms_user where id =' . $_var_17 . '';
	$_var_19 = mysql_query($_var_18);
	$_var_20 = mysql_fetch_array($_var_19);
	return $_var_20['aik_user_img']; 
}
function get_admin_img()
{
	$_var_21 = 'select * from aikcms_basic where id =1';
	$_var_22 = mysql_query($_var_21);
	$_var_23 = mysql_fetch_array($_var_22);
	return $_var_23['aik_admin_img']; 
}
function get_user_name($_var_24)
{
	$_var_25 = 'select * from aikcms_user where id =' . $_var_24 . '';
	$_var_26 = mysql_query($_var_25);
	$_var_27 = mysql_fetch_array($_var_26);
	return $show_name = $_var_27['aik_user_alias']=="" ? $_var_27['aik_user_name'] : $_var_27['aik_user_alias']; 
}
function get_user_int($_var_28)
{
	$_var_29 = 'select * from aikcms_user where id =' . $_var_28 . '';
	$_var_30 = mysql_query($_var_29);
	$_var_31 = mysql_fetch_array($_var_30);
	return $_var_31['aik_user_int']; 
}
function get_user_allmoney($_var_32)
{
	$_var_33 = 'select * from aikcms_user where id =' . $_var_32 . '';
	$_var_34 = mysql_query($_var_33);
	$_var_35 = mysql_fetch_array($_var_34);
	return $_var_35['aik_user_allmoney']; 
}

function get_gbook_content($_var_36)
{
	$_var_37 = 'select * from aikcms_gbook where id =' . $_var_36 . '';
	$_var_38 = mysql_query($_var_37);
	$_var_39 = mysql_fetch_array($_var_38);
	return $_var_39['aik_gbook_content']; 
}

function get_gbook_reply($_var_40)
{
	$_var_41 = 'select * from aikcms_gbook where aik_gbook_reply =' . $_var_40 . '';
	$_var_42 = mysql_query($_var_41);
	$_var_43 = mysql_fetch_array($_var_42);
	if ($_var_43) {
		return '已回复';
	} 
}

function aik_gg($_var_44)//gg不屏蔽
{
	$_var_45 = 'select * from aikcms_ad where id =' . $_var_44 . '';
	$_var_46 = mysql_query($_var_45);
	$_var_47 = mysql_fetch_array($_var_46);
	return htmlspecialchars_decode($_var_47['aik_ad_code']); 
}

function aik_user_usergrouplength($_var_48)
{
	$_var_49 = 'select * from aikcms_usergroup where id =' . $_var_48 . '';
	$_var_50 = mysql_query($_var_49);
	$_var_51 = mysql_fetch_array($_var_50);
	return $_var_51['aik_usergroup_length']; 
}
function aik_gbook_usergroupid($_var_52)
{
	$_var_53 = 'select * from aikcms_user where id =' . $_var_52 . '';
	$_var_54 = mysql_query($_var_53);
	$_var_55 = mysql_fetch_array($_var_54);
	return $_var_55['aik_user_group']; 
}
function aik_user_usergroupid($_var_56)
{
	$_var_57 = 'select * from aikcms_user where aik_user_name ="' . $_var_56 . '"';
	$_var_58 = mysql_query($_var_57);
	$_var_59 = mysql_fetch_array($_var_58);
	return $_var_59['aik_user_group']; 
}
function aik_user_usergroup($_var_60)
{
	if(aik_user_usergroupid($_var_60)==0){return '普通会员';}else{
	$_var_61 = 'select * from aikcms_usergroup where id =' . aik_user_usergroupid($_var_60) . '';
	$_var_62 = mysql_query($_var_61);
	$_var_63 = mysql_fetch_array($_var_62);
	return '<font style="font-weight:bold">'.$_var_63['aik_usergroup_name'].'</font>'; }
}
function aik_user_usergroupimg($_var_64)
{
	if(aik_user_usergroupid($_var_64)<>0){
	$_var_65 = 'select * from aikcms_usergroup where id =' . aik_user_usergroupid($_var_64) . '';
	$_var_66 = mysql_query($_var_65);
	$_var_67 = mysql_fetch_array($_var_66);
	return '<img src="'.$_var_67['aik_usergroup_img'].'" style="margin-left:10px;height:18px;margin-top: -6px;">'; }
}
function aik_gbook_usergroupimg($_var_68)
{   
    if(aik_gbook_usergroupid($_var_68)<>0){
	$_var_69 = 'select * from aikcms_usergroup where id =' . aik_gbook_usergroupid($_var_68) . '';
	$_var_70 = mysql_query($_var_69);
	$_var_71 = mysql_fetch_array($_var_70);
	return '<img src="'.$_var_71['aik_usergroup_img'].'" style="margin-left:6px; height:14px;margin-top: -4px;">'; }
}

function aik_nav_name($_var_72)
{
	$_var_73 = 'select * from aikcms_nav where id =' . $_var_72. '';
	$_var_74 = mysql_query($_var_73);
	$_var_75 = mysql_fetch_array($_var_74);
	if($_var_72==0){
	return "一级导航";
	}else{
	return $_var_75['aik_nav_name']; 
	}
}
function aik_nav_id($_var_76)
{
	$_var_77 = 'select * from aikcms_nav where aik_nav_name ="' . $_var_76 . '"';
	$_var_78 = mysql_query($_var_77);
	$_var_79 = mysql_fetch_array($_var_78);
	return $_var_79['id']; 
}
function aik_nav_babyname($_var_80)
{
	$_var_83 = '';
	$_var_81 = mysql_query('select * from aikcms_nav where aik_nav_papa =' . aik_nav_id($_var_80). '');
	while (!!($_var_82 = mysql_fetch_array($_var_81))) {
		$_var_83 .= $_var_82['aik_nav_name'];
	}
	return $_var_83;
}
function aik_navselect_list($_var_84)
{
	$_var_85 = 'select * from aikcms_nav where aik_nav_papa  = "0" order by id asc';
	$_var_86 = mysql_query($_var_85);
	while ($_var_87 = mysql_fetch_array($_var_86)) {
		$_var_88 = $_var_87['id'] == $_var_84 ? 'selected="selected"' : '';
		$_var_89 .= $_var_88.'<option value="' . $_var_87['id'] . '" ' . $_var_88 . '>' . $_var_87['aik_nav_name'] . '</option>';
	}
	return $_var_89;
}

function aik_nav_baby($_var_90)
{
	$_var_91 = mysql_query('SELECT * from aikcms_nav where  aik_nav_papa = '.$_var_90.' ');
	$_var_92 = mysql_num_rows($_var_91);
	return $_var_92; 
}
function aik_video_num($_var_93)
{
	$_var_94 = mysql_query('SELECT * from aikcms_video where  aik_video_group = '.$_var_93.' ');//你的查询
	$_var_95 = mysql_num_rows($_var_94);
    return $_var_95;
}
function aik_video_usergroup($_var_96)
{
	$_var_99 = '';
	$_var_97 = mysql_query('SELECT * from aikcms_usergroup where  id  in  (' . $_var_96 . ')');
	
	while ($_var_98 = mysql_fetch_array($_var_97)) {
		$_var_99 .= $_var_98['aik_usergroup_name'].' / ';
	}
    return $_var_99;
}
function aik_videoselect_usergroup($_var_100)
{
	$_var_105 = '';
	$_var_101 = 'select * from aikcms_usergroup  order by id asc';
	$_var_102 = mysql_query($_var_101);
	while ($_var_103 = mysql_fetch_array($_var_102)) {
		$_var_104 = in_array($_var_103['id'], explode(",", $_var_100)) ? 'checked' : '';
		$_var_105 .= '<input name="usergroup[]" class="tuijian" value="'.$_var_103['id'].'"  title="'.$_var_103['aik_usergroup_name'].'"  '.$_var_104.' type="checkbox"><div class="layui-unselect layui-form-checkbox"><span>'.$_var_103['aik_usergroup_name'].'</span><i class="layui-icon"></i></div>';
	}
	return $_var_105;
}
function get_video_usergroup($_var_106)
{
	$_var_107 = mysql_query('SELECT * from aikcms_videogroup where  id = '.$_var_106.' ');
	$_var_108 = mysql_fetch_array($_var_107);
	return $_var_108['aik_videogroup_usergroup']; 
}
function colrule_select_list($_var_109)
{
	$_var_110 = 'select * from aikcms_colgroup order by id asc';
	$_var_111 = mysql_query($_var_110);
	while ($_var_112 = mysql_fetch_array($_var_111)) {
		$_var_113 = $_var_112['aik_colgroup_cjurl'] == $_var_109 ? 'selected="selected"' : '';
		$_var_114 .= $_var_113.'<option value="' . $_var_112['aik_colgroup_cjurl'] . '" ' . $_var_113 . '>' . $_var_112['aik_colgroup_name'] . '</option>';
	}
	return $_var_114;
}

function aik_video_buy($_var_115,$_var_116)
{   
	$_var_117 = mysql_query('select * from aikcms_user_order where aik_order_videoid='.$_var_115.' and aik_order_userid='.$_var_116.'');
	if(mysql_fetch_array($_var_117 )){
	return true;  //观看过、购买过
	}else{
	return false; 	
}
}

function get_usergroup_name($_var_118)
{
	$_var_119 = mysql_query('select * from aikcms_usergroup where id=' . $_var_118 . '');
	if (!!($_var_120 = mysql_fetch_array($_var_119))) {
		return '<font style="color:red">'.$_var_120['aik_usergroup_name'].'</font>';
	} else {
		return '普通会员';
	}
}
function get_videogroup_name($_var_121)
{
	$_var_122 = mysql_query('select * from aikcms_videogroup where id=' . $_var_121 . '');
	if (!!($_var_123 = mysql_fetch_array($_var_122))) {
		return $_var_123['aik_videogroup_name'];
	} else {
		return '';
	}
}
function get_colgroup_name($_var_124)
{
	$_var_125 = mysql_query('select * from aikcms_colgroup where id=' . $_var_124 . '');
	if (!!($_var_126 = mysql_fetch_array($_var_125))) {
		return $_var_126['aik_colgroup_name'];
	} else {
		return '';
	}
}
function getPageHtml($_var_127, $_var_128, $_var_129)
{
	$_var_130 = 5;
	$_var_127 = $_var_127 < 1 ? 1 : $_var_127;
	$_var_127 = $_var_127 > $_var_128 ? $_var_128 : $_var_127;
	$_var_128 = $_var_128 < $_var_127 ? $_var_127 : $_var_128;
	$_var_131 = $_var_127 - floor($_var_130 / 2);
	$_var_131 = $_var_131 < 1 ? 1 : $_var_131;
	$_var_132 = $_var_127 + floor($_var_130 / 2);
	$_var_132 = $_var_132 > $_var_128 ? $_var_128 : $_var_132;
	$_var_133 = $_var_132 - $_var_131 + 1;
	if ($_var_133 < $_var_130 && $_var_131 > 1) {
		$_var_131 = $_var_131 - ($_var_130 - $_var_133);
		$_var_131 = $_var_131 < 1 ? 1 : $_var_131;
		$_var_133 = $_var_132 - $_var_131 + 1;
	}
	if ($_var_133 < $_var_130 && $_var_132 < $_var_128) {
		$_var_132 = $_var_132 + ($_var_130 - $_var_133);
		$_var_132 = $_var_132 > $_var_128 ? $_var_128 : $_var_132;
	}
	if ($_var_127 > 1) {
		$_var_134 .= '<li><a  title="上一页" href="' . $_var_129 . ($_var_127 - 1) . '&page=' . ($_var_127 - 1) . '"">上一页</a></li>';
	}
	for ($_var_135 = $_var_131; $_var_135 <= $_var_132; $_var_135++) {
		if ($_var_135 == $_var_127) {
			$_var_134 .= '<li><a style="background:#1a9cd6;"><font color="#fff">' . $_var_135 . '</font></a></li>';
		} else {
			$_var_134 .= '<li><a href="' . $_var_129 . $_var_135 . '&page=' . $_var_135 . '">' . $_var_135 . '</a></li>';
		}
	}
	if ($_var_127 < $_var_132) {
		$_var_134 .= '<li><a  title="下一页" href="' . $_var_129 . ($_var_127 + 1) . '&page=' . ($_var_127 + 1) . '"">下一页</a></li>';
	}
	return $_var_134;
}

// 防注入相关代码
//Code By Safe3 
//Add HTTP_REFERER by D.
$referer=empty($_SERVER['HTTP_REFERER']) ? array() : array($_SERVER['HTTP_REFERER']);
function customError($errno, $errstr, $errfile, $errline)
{ 
 echo "<b>Error number:</b> [$errno],error on line $errline in $errfile<br />";
 die();
}
set_error_handler("customError",E_ERROR);
$getfilter="'|\\b(and|or)\\b.+?(>|<|=|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
$postfilter="\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
$cookiefilter="\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
function StopAttack($StrFiltKey,$StrFiltValue,$ArrFiltReq){  

$StrFiltValue=arr_foreach($StrFiltValue);
if (preg_match("/".$ArrFiltReq."/is",$StrFiltKey)==1){   
        slog("<br><br>操作IP: ".$_SERVER["REMOTE_ADDR"]."<br>操作时间: ".strftime("%Y-%m-%d %H:%M:%S")."<br>操作页面:".$_SERVER["PHP_SELF"]."<br>提交方式: ".$_SERVER["REQUEST_METHOD"]."<br>提交参数: ".$StrFiltKey."<br>提交数据: ".$StrFiltValue);
        print "<div style=\"position:fixed;top:0px;width:100%;height:100%;background-color:white;color:green;font-weight:bold;border-bottom:5px solid #999;\"><br>您的提交带有不合法参数,谢谢合作!<br><br>了解更多请点击:<a href=\"http://webscan.360.cn\">http://webscan.360.cn</a></div>";
        exit();
}  
}  
//$ArrPGC=array_merge($_GET,$_POST,$_COOKIE);
foreach($_GET as $key=>$value){ 
	StopAttack($key,$value,$getfilter);
}
foreach($_POST as $key=>$value){ 
	StopAttack($key,$value,$postfilter);
}
foreach($_COOKIE as $key=>$value){ 
	StopAttack($key,$value,$cookiefilter);
}
foreach($referer as $key=>$value){ 
  StopAttack($key,$value,$getfilter);
}
function slog($logs)
{
  $toppath=$_SERVER["DOCUMENT_ROOT"]."/log.htm";
  $Ts=fopen($toppath,"a+");
  fputs($Ts,$logs."\r\n");
  fclose($Ts);
}
function arr_foreach($arr) {
  static $str;
  if (!is_array($arr)) {
  return $arr;
  }
  foreach ($arr as $key => $val ) {

    if (is_array($val)) {

        arr_foreach($val);
    } else {

      $str[] = $val;
    }
  }
  return implode($str);
}
function show_error($msg,$httpcode='404'){
	
	if($httpcode=='404'){
		@header("http/1.1 404 not found");
   		@header("status: 404 not found");
	}
	$html = '<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Error Code '.$httpcode.'</title>
<style type="text/css">
::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }
body {background-color: #fff;margin: 40px;font: 13px/20px normal Helvetica, Arial, sans-serif;color: #4F5155;}
a {color: #003399;background-color: transparent;font-weight: normal;}
h1 {color: #444;background-color: transparent;border-bottom: 1px solid #D0D0D0;font-size: 19px;font-weight: normal;margin: 0 0 14px 0;padding: 14px 15px 10px 15px;}
code {font-family: Consolas, Monaco, Courier New, Courier, monospace;font-size: 12px;background-color: #f9f9f9;border: 1px solid #D0D0D0;color: #002166;display: block;margin: 14px 0 14px 0;padding: 12px 10px 12px 10px;}
#container {margin: 10px;border: 1px solid #D0D0D0;box-shadow: 0 0 8px #D0D0D0;}

p {margin: 12px 15px 12px 15px;}
</style>
</head>
<body>
	<div id="container"><h1>Error Code '.$httpcode.'</h1><p>'.$msg.'</p>	</div>
</body>
</html>
';
echo $html;exit;
}