<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel='stylesheet' id='main-css'  href='<?php echo $aik_domain;?>template/<?php echo $template;?>/css/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='main-css'  href='<?php echo $aik_domain;?>template/<?php echo $template;?>/css/play.css' type='text/css' media='all' />
<link href="./template/<?php echo $template;?>/css/404.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='http://apps.bdimg.com/libs/jquery/2.0.0/jquery.min.js?ver=0.5'></script>	
<title><?php echo $aik_title;?></title>
<meta name="keywords" content="<?php echo $aik_keywords;?>">
<meta name="description" content="<?php echo $aik_desc;?>">

</head>
<body class="apptop">
<?php include 'aik_head.php';?>
<div class="auto">
	<div class="container404">
		<div class="settings">
			<i class="icon"></i>
			<h4>很抱歉！没有找到您要访问的页面！</h4>
			<p><span id="num">5</span> 秒后将自动跳转到首页</p>
			<div>
				<a href="https://www.lanzous.com/b00t6jx2f" title="更多源码">更多源码</a>
				<a href="javascript:;" title="上一步" id="reload-btn">上一步</a>
			</div>
		</div>
	</div>
</div>

<script>
	//倒计时跳转到首页的js代码
	var $_num=$("#num");
	var num=parseInt($_num.html());
	var numId=setInterval(function(){
		num--;
		$_num.html(num);
		if(num===0){
			//跳转地址写在这里
			window.location.href="<?php echo $aik_domain;?>";
		}
	},1000);
	//返回按钮单击事件
	var reloadPage = $("#reload-btn");
	reloadPage.click(function(e){
		window.history.back();
	});
</script>


<?php include 'aik_foot.php';?>
</body></html>